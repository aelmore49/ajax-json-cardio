// Crud Operation via LOCAL NODE Server

// Elements
const findUserSection = document.querySelector(".find-user-section");
const cta = document.querySelector(".find-user-btn");
const input = document.querySelector(".find-user-input");

const bottomDiv = document.querySelector(".bottom-div");
const addUserSection = document.querySelector(".add-user-section");
const addUserCta = document.querySelector(".add-user-btn");
const output = document.querySelector(".output");
const formCta = document.createElement("button");
const searchSelect = document.createElement("select");
const viewAllCta = document.createElement("button");
const pageCtaHolder = document.createElement("div");

// Css Classes to apply
const hide = `display-none`;
const invalid = `invalid`;

// Configure Elements
cta.classList.add(hide);
input.classList.add(hide);

addUserCta.textContent = `Add a user`;
formCta.setAttribute("type", "submit");
formCta.textContent = "Add User";
formCta.classList.add("form-btn");
searchSelect.classList.add("search-select-menu");
viewAllCta.classList.add("view-all-btn");
viewAllCta.textContent = "View Users";
addUserSection.append(viewAllCta);
pageCtaHolder.classList.add("button-holder");

// An array with options for searching users
const searchOptionsArray = [
	`Select A User Search Method`,
	`Search By User Id`,
	`Search By User First Name`,
	`Search By User Last Name`,
	`Search By User Email`,
];

//Global page object
const pageObject = { json: {}, start: 0, itemsPerPage: 10, arr: [] };

// Populate select menu with options from the searchOptionsArray
searchOptionsArray.forEach((option, idx) => {
	let searchOption = createHtmlElement(
		searchSelect,
		"option",
		option,
		"search-option",
		`search-option-${idx + 1}`,
		option.toLocaleLowerCase()
	);
});

findUserSection.prepend(searchSelect);

// Select menu event listener
searchSelect.addEventListener("change", (e) => {
	const target = e.target.value;
	if (target !== `Select A User Search Method`) {
		switch (target) {
			case `Search By User Id`:
				commenceSearch("id");
				break;
			case `Search By User First Name`:
				commenceSearch("fName");
				break;
			case `Search By User Last Name`:
				commenceSearch("lName");
				break;
			case `Search By User Email`:
				commenceSearch("email");
				break;
		}
	}
});

// Function that configures and displays the user selected search method
function commenceSearch(type) {
	if (type === "id") {
		cta.textContent = "Find User By ID ";
		input.setAttribute("type", "number");
		input.setAttribute("placeholder", "Enter User Id");
		input.setAttribute("data-type", "ID");
		cta.classList.remove(hide);
		input.classList.remove(hide);
	}
	if (type === "fName") {
		cta.textContent = "Find User By First Name";
		input.setAttribute("type", "text");
		input.setAttribute("placeholder", "Enter User's First Name");
		input.setAttribute("data-type", "First Name");
		cta.classList.remove(hide);
		input.classList.remove(hide);
	}
	if (type === "lName") {
		cta.textContent = "Find User By Last Name";
		input.setAttribute("type", "text");
		input.setAttribute("placeholder", "Enter User's Last Name");
		input.setAttribute("data-type", "Last Name");
		cta.classList.remove(hide);
		input.classList.remove(hide);
	}
	if (type === "email") {
		cta.textContent = "Find User By Email";
		input.setAttribute("type", "text");
		input.setAttribute("placeholder", "Enter User's Email");
		input.setAttribute("data-type", "Email");
		cta.classList.remove(hide);
		input.classList.remove(hide);
	}
}

// API URL
const baseUrl = `http://localhost:3000/users`;

// Global window event listener to load DOM when elements ready
window.addEventListener("DOMContentLoaded", (e) => {
	console.log("Ready to Roll!");
	// Check if already have user data to determine if a fetch is necessary
	fetchData(baseUrl);
});

// Function that returns an object with the same structure as the data object that gets returned back from the get fetch request
function setupObject() {
	let data = getLocalData();
	let keyArray = [];
	let initialObj = {};
	for (const i in data[0]) {
		keyArray.push(i);
	}
	const singleObject = keyArray.reduce(
		(preValue, currValue, index) => ({
			...preValue,
			[currValue]: "",
		}),

		initialObj
	);
	return singleObject;
}

// Function that makes initial request and then call a function that saves returned data to local storage
function fetchData(url = baseUrl, method = "GET") {
	// make initial fetch request for user data
	fetch(url, { method })
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			// Call saveToLocalStorage function and pass it the returned data to save to local storage
			saveToLocalStorage(data);
			createPages(data);
		});
}
// Function that makes a post request with the data that the user enters in on the form
function postData(url, data, type) {
	let method = `${type}`;
	let headers = {
		"Content-Type": "application/json",
	};
	fetch(url, {
		method,
		headers,
		body: data,
	})
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			// Call saveToLocalStorage function and pass it the returned data to save to local storage
			saveToLocalStorage(data);
		});
}

//function that saves returned data to local storage
function saveToLocalStorage(data) {
	data !== null ? localStorage.setItem("userData", JSON.stringify(data)) : "";
}

// Function that returns local storage data for user
function getLocalData() {
	const localData = JSON.parse(localStorage.getItem("userData"));
	return localData;
}

// CTA click event listener that calls the search for user function, followed by the render data function
cta.addEventListener("click", (e) => {
	let inputValue = input.value;
	let searchType = input.getAttribute("data-type");
	if (inputValue === "") {
		alert(`Please Enter the User's ${searchType}`);
		input.classList.add(invalid);
	} else {
		output.innerHTML = "";
		const id = searchType === "ID" ? +inputValue : inputValue;
		const localData = getLocalData();
		if (localData.length) {
			if (input.classList.contains(invalid)) {
				input.classList.remove(invalid);
			}
			let findUser = searchForUser(id, localData, searchType);
			if (typeof findUser !== "string") {
				renderData(findUser);
				input.value = "";
			} else {
				alert(findUser);
				input.value = "";
			}
		} else {
			alert("No User Found!");
		}
	}
});

// Function that searches for a user and returns the search results
function searchForUser(id, localData, type) {
	let foundUser = [];
	let method = "";
	localData.forEach((info) => {
		switch (type) {
			case `ID`:
				method = info.id;
				id = +input.value;
				break;
			case `First Name`:
				method = info.first_name;
				break;
			case `Last Name`:
				method = info.last_name;
				break;
			case `Email`:
				method = info.email;
				break;
		}
		let user = id === method ? info : "Not found";
		if (user !== "Not found") {
			foundUser.push(user);
		}
	});
	if (foundUser.length >= 1) {
		return foundUser;
	} else {
		return `No user with a ${type} of ${id} was found. Please try again`;
	}
}

// Function that renders the user's data to the DOM
function renderData(data, index = 0) {
	output.innerHTML = "";
	const userBtns = Array.from(document.querySelectorAll(".pagination-btn"));
	userBtns.forEach((btn) => {
		if (+btn.id === index) {
			btn.classList.add("active");
		} else {
			btn.classList.remove("active");
		}
	});

	data.forEach((user) => {
		const content = `
        <h2>First Name: <span>${user.first_name}</span></h2>
        <h2>Last Name: <span>${user.last_name}</span></h2>
        <h2>Id: ${user.id}</h2>
        <h2>Email: ${user.email}</h2>
        <h2>Gender: <span>${user.gender}</span></h2>
        `;
		const userInfo = createHtmlElement(output, "div", content, "user-info");

		const updateCta = createHtmlElement(
			userInfo,
			"button",
			"Update",
			"user-btn-update",
			`update-${user.id}`
		);

		const deleteCta = createHtmlElement(
			userInfo,
			"button",
			"Delete",
			"user-btn-delete",
			`delete-${user.id}`
		);

		updateCta.addEventListener("click", (e) => {
			getupateCta(e.target, deleteCta, user.email);
		});

		deleteCta.addEventListener("click", (e) => {
			editHandlerCta(e.target, "DELETE");
		});
	});
}

function getupateCta(cta, deleteCta, email) {
	const editFields = cta.parentNode.querySelectorAll("span");
	const parent = cta.parentNode;
	const userEmail = email;
	editFields.forEach((span) => {
		span.contentEditable = true;
		span.classList.add("editable");
	});

	const confirmCta = createHtmlElement(
		parent,
		"button",
		"Cofirm Changes",
		"confirm-edit-cta"
	);
	deleteCta.classList.add(hide);

	confirmCta.addEventListener("click", (e) => {
		editFields.forEach((span) => {
			span.contentEditable = false;
			span.classList.remove("editable");
			deleteCta.classList.remove(hide);

			//makePostable(parent, "PUT");
		});

		let postObject = {
			first_name: editFields[0].textContent,
			last_name: editFields[1].textContent,
			gender: editFields[2].textContent,
			email: userEmail,
		};

		console.log(postObject);
		makePostable(postObject, "PUT");
	});
}

const editHandlerCta = function (cta, type) {
	const id = cta.id.split("-").pop();
	const url = `${baseUrl}/${id}`;
	fetchData(url, `${type}`);
};

// Function that creates and appends a HTML element to the DOM
function createHtmlElement(
	parentDiv,
	elementType,
	content,
	cssClass,
	id = "",
	value = ""
) {
	let element = document.createElement(elementType);
	element.innerHTML = `${content}`;
	element.classList.add(cssClass);
	if (id !== "") {
		element.id = id;
	}
	parentDiv.append(element);
	return element;
}

// Add event click listener for addUserCta
addUserCta.addEventListener("click", (e) => {
	output.innerHTML = "";
	findUserSection.classList.add(hide);
	viewAllCta.textContent = "Return Home";
	createForm();
});

// Function that creates a form when addUserCta is clicked
function createForm() {
	let objectStructure = setupObject();
	let inputCount = Object.keys(objectStructure).length;
	let inputs = "";
	let labels = Object.keys(objectStructure);
	let placeHolders = labels
		.join(" ")
		.toUpperCase()
		.replaceAll("_", "-")
		.split(" ");

	for (let i = 0; i < inputCount; i++) {
		inputs += `
		<label for="${labels[i]}">${placeHolders[i]}:</label><br>
		<input placeholder="${placeHolders[i]}" class="user-input" type="${
			placeHolders[i] === "ID" ? "number" : "text"
		}" id="${labels[i]}" name="${labels[i]}"><br>`;
	}

	const form = createHtmlElement(
		output,
		"form",
		inputs,
		"form-section",
		"userForm"
	);
	formCta.setAttribute("form", "userForm");
	form.append(formCta);

	addUserCta.classList.add(hide);

	// Event listener for when the form cta is clicked
	formCta.addEventListener("click", (e) => {
		e.preventDefault();
		const isValid = validateFormInupt(form);
		if (isValid) {
			makePostable(form, "POST");
		}
	});
}

// Function that validates all the form input fields.
function validateFormInupt(form) {
	const formArray = Array.from(form);
	let validEmail = false;
	let validId = false;
	formArray.forEach((input) => {
		if (input.classList.contains("user-input")) {
			if (input.value === "") {
				alert(`Please fill out the User's "${input.placeholder}" field`);
				input.classList.add(invalid);
				return;
			} else if (input.value !== "") {
				if (input.classList.contains(invalid)) {
					input.classList.remove(invalid);
				}
				if (input.id === "id") {
					const id = input.value;
					const hasId = isAvailable(+id, "id");
					if (hasId !== -1) {
						alert(
							`The id "${id}" is not available. Please select a different user Id`
						);
						input.value = "";
						return;
					} else {
						validId = true;
					}
				} else if (input.id === "email") {
					const email = input.value;
					const hasEmail = isAvailable(email, "email");
					if (hasEmail !== -1) {
						alert(
							`There is already a user with the email address "${email}". Please enter a different email address for the user.`
						);
						input.value = "";
						return;
					} else {
						validEmail = true;
					}
				}
			}
		}
	});
	if (validEmail && validId) {
		return true;
	} else {
		return false;
	}
}
// Function that checks to see if specific form fields are available for the user to use when adding a user
function isAvailable(input, type) {
	const data = Array.from(getLocalData());
	const isTaken = data.findIndex((el) => el[type] === input);
	return isTaken;
}

// Function that preps data for post request
function makePostable(data, type) {
	const keyArray = Object.keys(setupObject());
	const formData = Array.from(data);
	console.log(formData);
	let valueArray = [];
	formData.forEach((item) => {
		if (item.type === "number") {
			const numberVal = +item.value;
			valueArray.push(numberVal);
		} else if (item.type !== "submit" && item.type !== "number") {
			valueArray.push(item.value.trim());
		}
	});
	let postObject = {};
	postObject = keyArray.reduce(
		(pre, cur, idx) => ({ ...pre, [cur]: valueArray[idx] }),
		{}
	);

	console.log(postObject);
	//postData(baseUrl, JSON.stringify(postObject), type);
}

// function that creates pages that hold ten users per page.
const createPages = (data) => {
	pageObject.arr.length = 0;
	for (let i = 0; i < data.length; i += pageObject.itemsPerPage) {
		let tempArr = data.slice(i, i + pageObject.itemsPerPage);
		pageObject.arr.push(tempArr);
	}
};

// Function for creating user page pagination buttons.
const createPageBtns = (data) => {
	pageObject.arr.forEach((btn, index) => {
		let userPaginationCta = createHtmlElement(
			pageCtaHolder,
			"button",
			`${index + 1}`,
			"pagination-btn",
			`${index}`
		);

		userPaginationCta.addEventListener("click", (e) => {
			output.innerHTML = "";

			renderData(pageObject.arr[index], index);
		});
	});
	bottomDiv.prepend(pageCtaHolder);
	// Set inital active state to 1st pagination button
	let first = document.getElementById("0");
	first.classList.add("active");
};

// View Users CTA event listener
viewAllCta.addEventListener("click", (e) => {
	if (viewAllCta.textContent !== "View Users") {
		viewAllCta.textContent = "View Users";
	}

	if (findUserSection.classList.contains(hide)) {
		findUserSection.classList.remove(hide);
	}

	if (addUserCta.classList.contains(hide)) {
		addUserCta.classList.remove(hide);
	}

	renderData(pageObject.arr[0]);
	const data = getLocalData();
	pageCtaHolder.innerHTML = "";
	createPageBtns(data);
});
