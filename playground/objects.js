
// Object Cardio Refresher

const person = {
    firstname: "Alex",
    lastname: "Elmore",
    age: 36,
    interests: ['JavaScript', 'React', 'TypeScript'],
    "courses": [
        {
            "name": "JS",
            "length": 20
        },
        {
            "name": "TypeScript",
            "length": 10
        },
        {
            "name": "React",
            "length": 15
        },
        {
            "name": "AEM",
            "length": 20
        }
    ],

};


class Developer {
    constructor(skills, stack) {
        this.skills = skills,
            this.stack = stack;
        this.getInfo = this.getInfo.bind(this);
    }
    getInfo = () => {
        return `A ${this.stack} must know have knowledge of ${this.skills}.`;
    };
}

const frontEnd = new Developer('JavaScript', 'FrontEnd');
const backEnd = new Developer('either Java, PHP or c#', 'BackEnd');

console.log(frontEnd.getInfo());
console.log(backEnd.getInfo());


console.log(person["courses"][0]["length"]);


const course = person["courses"];

course.forEach((c) => {
    console.log(c.name);
});

console.log(course);


for (const prop in person) {
    console.log(prop);
}

const keys = Object.keys(person);

console.log(keys);

keys.forEach((key) => {
    console.log(key);
});

const values = Object.values(person);
console.log(values);

values.forEach((val) => {
    console.log(val);
});


const entries = Object.entries(person);
console.log(entries);


for (const arr of entries) {
    console.log(arr);
}