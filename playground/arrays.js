// Array Cardio Refresher 
const arr1 = ["one", 1, true, []];

const arr2 = arr1;

const arr3 = [];

const arr4 = [...arr1];

const arr5 = arr1.map(el => el);

arr1.forEach((el, index) => {
    arr3[index] = el;
});

arr1[2] = 'test';

arr3[1] = '69';

arr4[3] = 'tennis';

arr2.push(420);

arr1.push('code');

arr3.pop();

arr4.shift();

arr3.unshift('ME FIRST');

console.log('arr1:', arr1);

console.log('arr2:', arr2);

console.log('arr3:', arr3);

console.log('arr4:', arr4);

console.log('arr5:', arr5);