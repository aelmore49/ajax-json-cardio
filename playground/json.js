// JSON Refresher
const url = 'j1.json';
let data = '';
const localData = localStorage.getItem("me");

function myJson() {
    fetch(url)
        .then(res => res.text())
        .then(json => {
            data = JSON.parse(json);
            console.log(data);
            let str = JSON.stringify(data);
            localStorage.setItem("me", str);
        });
}

myJson();

console.log(localData);