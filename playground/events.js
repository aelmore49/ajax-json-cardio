// Events & DOM Elements Refresher

const divs = document.querySelectorAll("div");
const inputValue = document.querySelector(".myInput");
const hTag = document.querySelector("h1");
const span = document.querySelector("span");

inputValue.style.fontSize = "2rem";
let counter = 0;

const btn = document.createElement("button");
btn.textContent = "click me!";
btn.addEventListener("click", (e) => {
    counter++;
    const newDiv = document.createElement("div");
    newDiv.textContent = `${inputValue.value}: ${counter}`;
    newDiv.style.background = "gray";
    newDiv.style.color = "orange";
    document.body.append(newDiv);
    newDiv.addEventListener('click', myClick);
});
const val1 = span.append(btn);
const val2 = span.appendChild(btn);


inputValue.addEventListener("click", () => {
    inputValue.getAttribute("type") === "text" ? inputValue.setAttribute("type", "number") : inputValue.setAttribute("type", "text");
});

divs.forEach((div, index) => {

    div.innerHTML = `<h2>Hello World ${index + 1}</h2>`;
    div.addEventListener('click', myClick);

});

function myClick(e) {
    e.target.classList.toggle("box");
}

hTag.addEventListener("click", (e) => {

    if (hTag.textContent == 'JavaScript') {
        hTag.textContent = 'test';
        hTag.style.color = "purple";
        hTag.style.background = "teal";

    } else {
        hTag.textContent = 'JavaScript';
        hTag.style.color = "gold";
        hTag.style.background = "black";
    }
});

