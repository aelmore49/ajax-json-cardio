// GitHub Search API App

// File object containing name of local json file
const fileObject = {
	json: `local.json`,
};

// URL for GitHub API Endpoint
const url = `https://api.github.com/repos/twbs/bootstrap`;
const url2 = `https://api.github.com/orgs/octo-org/repos`;
const searchUrl = `https://api.github.com/search/issues?q="test"`;

// Elements
const output = document.querySelector(".output");
const input = document.querySelector(".input");
const cta = document.querySelector(".btn");

// Element Configuration
cta.textContent = "Search";

// Function for CTA clicks
function ctaHandler(e) {
	// Send initial fetch request to GitHub API
	fetch(searchUrl)
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			// Call function to output returned API data to page
			renderContent(data);
		})
		.catch((error) => {
			console.log(error);
		});
}

// Function for rendering returned API data to the DOM
function renderContent(data) {
	console.log(data);
}

// Event Handler for CTA clicks
cta.addEventListener("click", ctaHandler);
