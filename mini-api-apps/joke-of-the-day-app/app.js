// Joke Of The Day App

// Elements
const primaryButton = document.querySelector(".btn");
let primaryOutput = document.querySelector(".output");
const primaryInput = document.querySelector(".val");
const goBackCta = document.createElement("button");

// Configure Elements
primaryButton.textContent = "Get Joke";
primaryButton.classList.add("btn");
primaryButton.classList.add("start-button");
primaryInput.classList.add("display-none");
goBackCta.textContent = "Go Back";
goBackCta.classList.add("btn");
primaryOutput.classList.add("display-none");

// API URL
const url = `https://api.jokes.one/jod`;

// Function to make initial fetch request to the API URL
const makeInitialRequest = () => {
	fetch(url)
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			// If data.succes.total is greater than 0, we know we can proceed.
			if (data.success.total > 0)
				// Call function to that sets up the page
				setUpPage(data);
		})
		.catch((error) => {
			console.log(error);
		});
};

// Function that creates page output using joke content
const setUpPage = (data) => {
	// Display output div
	primaryOutput.classList.add("joke-container");
	primaryOutput.classList.remove("display-none");

	// Destructor data object
	const { contents } = data;
	const { jokes } = contents;

	// Loop through joke array
	jokes.forEach((joke) => {
		// Joke elements and calls to function to display the content to the DOM
		const description = `Title: ${joke.joke.title}`;
		outputToPage("h2", primaryOutput, description, "joke-title");

		const jokeData = `Date Created: ${joke.date}`;
		outputToPage("h3", primaryOutput, jokeData, "joke-date");

		const content = joke.joke.text;
		outputToPage("p", primaryOutput, content, "joke-text");
	});

	// Hide primary button and show the "Go Back" CTA
	primaryButton.classList.add("display-none");
	primaryButton.classList.remove("btn");
	primaryButton.classList.remove("start-button");
	primaryOutput.append(goBackCta);

	// Add event listener to the "Go Back" CTA
	goBackCta.addEventListener("click", () => {
		// Clear output div's content, display primary button, hide "Go Back" button and hide joke container
		primaryOutput.innerHTML = "";
		primaryOutput.classList.remove("joke-container");
		primaryOutput.classList.add("display-none");
		primaryButton.classList.remove("display-none");
		primaryButton.classList.add("btn");
		primaryButton.classList.add("start-button");
	});
};

// Function that outputs joke to the DOM
const outputToPage = (element, parent, html, cssClass) => {
	const el = document.createElement(element);
	el.innerHTML = html;
	el.classList.add(cssClass);
	parent.append(el);
};

// Event handler for button
primaryButton.addEventListener("click", makeInitialRequest);
