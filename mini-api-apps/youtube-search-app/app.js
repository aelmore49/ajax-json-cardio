// YouTube API Search App

// Elements
const cta = document.querySelector(".btn");
const input = document.querySelector(".val");
const output = document.querySelector(".output");
const dropdownHolder = document.createElement("div");
const dropdownCta = document.createElement("button");
const desiredResults = document.createElement("div");
const infoContainer = document.createElement("div");
const resultsHeader = document.createElement("h2");
let displayNumber = 5;
const maxResultsAllowed = 25;

// Function that creates and returns an Object which will hold user desired number of returned results
function createObject() {
	// Declare an empty object variable and populate it with numbers, 0 - max number allow, for user to select as desired number of results to return
	let tempNumber = {};
	for (var num = 0; num <= maxResultsAllowed; num++) {
		Object.defineProperty(tempNumber, `${num}`, {
			value: num,
			writable: true,
			enumerable: true,
			configurable: true,
		});
	}
	return tempNumber;
}

// Constant that holds reference to create object function
const tempObject = createObject();

// Element Configuration
cta.textContent = "Search YouTube";
input.setAttribute("placeholder", "Enter a search term");
dropdownHolder.classList.add("dropdown-holder");
dropdownCta.textContent = "Number of Results to Return";
dropdownCta.classList.add("results-cta");
desiredResults.classList.add("returned-results");
desiredResults.classList.add("no-show");
dropdownHolder.append(dropdownCta);
dropdownHolder.append(desiredResults);
input.parentNode.insertBefore(dropdownHolder, input);
output.append(infoContainer);

// Populate drowdown with number tempObject constant
for (const num in tempObject) {
	desiredResults.innerHTML += `
                                <a class="results-link" data-num=${num} href="#${tempObject[num]}">${num}</a>`;
}

// Event Listener for the dropdown CTA
dropdownCta.addEventListener("click", toggleDropdownMenu);

// Create array from result links
const resultLinks = Array.from(document.querySelectorAll(".results-link"));

// Loop through results array
resultLinks.forEach((link) => {
	link.addEventListener("click", (e) => {
		displayNumber = e.target.getAttribute("data-num");
		resultsHeader.textContent = `Results Number: ${displayNumber}`;
		output.parentNode.insertBefore(resultsHeader, output);
		toggleDropdownMenu();
	});
});

// Function to toggle "no-show" class for desiredResults div
function toggleDropdownMenu() {
	desiredResults.classList.toggle("no-show");
}

// API Key
const key = `AIzaSyAvw5_XUWYKry_X8UzfYs_XnxTom8CmOb4`;

// CTA Event Listener
cta.addEventListener("click", ctaHandler);

// Function that handles CTA clicks
function ctaHandler(e) {
	// URL
	const url = `https://youtube.googleapis.com/youtube/v3/search?part=snippet&maxResults=${displayNumber}&q=`;

	// Check for input value
	if (input.value) {
		// Clear out any existing content for the input container div
		infoContainer.innerHTML = "";
		if (infoContainer.classList.contains("info-container")) {
			infoContainer.classList.remove("info-container");
		}

		// Convert all input values to strings and make them lowercase and configure URL for fetch request
		const value = input.value.toString().toLowerCase();
		const tempKey = `&key=${key}`;
		const tempURL = `${url}${value}${tempKey}`;

		// Send inital fetch request
		fetch(tempURL)
			.then((res) => {
				return res.json();
			})
			.then((data) => {
				console.log(data);
				// Call function to render return results to the DOM
				displayResults(data);
			})
			.catch((error) => {
				console.log(error);
			});
	} else {
		alert("Please Enter a Term to Seach");
	}
}

// Function to render returned content to the DOM
function displayResults(data) {
	if (!infoContainer.classList.contains("info-container")) {
		infoContainer.classList.add("info-container");
	}

	// Pull out video items from data object
	const videoItems = data.items;

	// Search term to display with return results
	const searchTerm = input.value;

	infoContainer.innerHTML += `
    <h2 class="results">Results for: "${searchTerm}"</h2><br>`;

	// Clear input value
	input.value = "";

	// Loop through video items array
	videoItems.forEach((video, index) => {
		//Pull out video id for iframe src attribute
		const { id, snippet } = video;
		const { videoId } = id;
		const { title, description } = snippet;

		// Set iframe src attribute
		const src = `https://www.youtube.com/embed/${videoId}`;

		// Make infoContainer div element's innerhtml iframes of return search results
		infoContainer.innerHTML += `<hr class="video-hr">
                            <h2><i>Result ${index + 1} of ${
			videoItems.length
		}</i></h2>
        <h2>Video Title: <i style="color:#a53b3b;">${title}</i></h2>
        <h3>Video Description:<i style="color:#a53b3b;"> ${description}</i></h3>
                            <div class="video-holder"> <iframe class="video" width="100%" height="700" src="${src}"></iframe></div>
                            `;
	});
}
