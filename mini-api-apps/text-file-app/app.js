// AJAX Text File App

// Elements
const btn = document.querySelector(".btn");
const outPut = document.querySelector(".output");
const selectMenu = document.createElement("select");

// Global Constants
const selectionText = "Select A File";

const options = {
	default: `${selectionText}`,
	first: "firstTextFile.txt",
	second: "secondTextFile.txt",
	third: "onlyJsonFile.json",
};

// Configure Elements
btn.textContent = "Load File Selection Menu";
btn.classList.add("btn");
selectMenu.classList.add("select-box");
outPut.classList.add("display-none");

// Add event listener for Primary CTA
btn.addEventListener("click", loadSelectionMenu);

// Function that generates the select menu
function loadSelectionMenu() {
	// Hide Primary CTA and insert select menu into the DOM
	btn.parentNode.insertBefore(selectMenu, btn);
	btn.classList.add("display-none");
	btn.classList.remove("btn");

	// Populate select menu with options from options object constant
	for (const option in options) {
		selectMenu.options.add(new Option(options[option], options[option]));
	}

	// Setup event listener for select menu
	selectMenu.addEventListener("change", makeInitialRequest);
}

// Function that gets called when select menu changes
function makeInitialRequest(e) {
	// Get value for selected option to use in the fetch request
	const url = e.target.value;

	// If selection is the first default option, return, else proceed with the fetch request
	if (url === selectionText) {
		return;
	} else {
		fetch(url).then((res) => {
			// Check if data is in json or text format and proceed accordingly
			const contentType = res.headers.get("content-type");
			if (contentType && contentType.indexOf("application/json") !== -1) {
				return res.json().then((data) => {
					// Loop through data and send content to be displayed.
					data.forEach((content) => {
						// Call output function with 1st argument being data to display to DOM
						// Call output function with 2nd argument being the data's fetch url to use for the file's title output
						outputContent(content.content, url);
					});
				});
			} else {
				return res.text().then((data) => {
					// Call output function with 1st argument being data to display to DOM
					// Call output function with 2nd argument being the data's fetch url to use for the file's title output
					outputContent(data, url);
				});
			}
		});
	}
}

// Function to output file information to the DOM
function outputContent(data, title) {
	// Clear output div's existing content, if any, and then display the incoming content.
	outPut.innerHTML = "";
	outPut.classList.remove("display-none");
	outPut.classList.add("output-container");
	// Create an element to contain and display the file's information
	const fileInfo = document.createElement("div");
	fileInfo.innerHTML = `<h2>File Name: ${title}</h2> <br>
                      <h2>File Content: ${data} </h2>`;

	// Append the element containing the file's information to the ouput div so it can be displayed to the DOM
	outPut.append(fileInfo);
}
