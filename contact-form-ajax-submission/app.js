// AJAX Form Submission Project

// Elements
const myForm = document.querySelector("#signup");
const output = document.querySelector(".output");
output.style.display = 'none';

// URL
const url = `https://script.google.com/macros/s/AKfycbzRchlxjctUIyQ1xQcAU6-eDbQhygVQTSFyNo0lN-8xPl6IqmfH2xzR8u_fAfe2ARbZ/exec`;

// Event listener for the form object
myForm.addEventListener("submit", (e) => {
    e.preventDefault();
    console.log("Sending data");
    const elements = myForm.elements;
    const holder = {};
    const err = [];
    let val = true;

    for (let i = 0; i < elements.length; i++) {
        const el = elements[i];
        if (el.getAttribute("type") === "submit") {
            val = false;
        }
        if (el.name === "user") {
            if (el.value.length <= 1) {
                val = false;
                err.push("Name greater than 1 characters");
                el.value = "";
            } else {
                holder[el.name] = el.value;
            }
        }
        if (el.name === "email") {
            if (checkEmail(el.value)) {
                val = true;
                holder[el.name] = el.value;
            } else {
                val = false;
                err.push("Email address entered is not a valid email address");
                el.value = "";
            }
        }
        if (el.name === "message") {
            if (el.value !== "") {
                holder[el.name] = el.value;
            } 
        }
    }
    if (err.length > 0) {
        output.innerHTML = '';
        output.style.display = 'block';
        err.forEach((e) => {
            output.innerHTML += `<h2>${e}</h2>`;
        });
    } else {
        if (output.innerHTML !== "") {
            output.innerHTML = "";
        }
        // form submission
        // Seutp fetch request
        for (let i = 0; i < elements.length; i++){
            const el = elements[i];
        }
        makeRequest(holder);
    }
});

// Function that checks validity of email address
const checkEmail = function (email) {
    const emailPattern =
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return emailPattern.test(email);
};

// Funtion that makes the fetch request
const makeRequest = function (data) {
    output.style.display = 'block';
    output.innerHTML = 'Sending...'
    fetch(url, {
        method: "POST",
        body: JSON.stringify(data),
    })
        .then((res) => res.json())
        .then((data) => {
            console.log(data);
            clearform()
        });
};

const clearform = function () {
    output.innerHTML = ""
    const elements = myForm.elements;
    output.style.display = 'none';
    for (let i = 0; i < elements.length; i++){
        const el = elements[i];
        if (el.getAttribute("type") !== "submit") {
            el.value = ""
        }
    }
}
