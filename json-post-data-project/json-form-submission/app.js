// AJAX Form Submission APP

// Elements
const cta = document.querySelector(".btn");
const input = document.querySelector(".val");
const output = document.querySelector(".output");
const topContent = document.querySelector(".top-content");

// Base API URL
const baseUrl = `https://script.google.com/macros/s/AKfycbzUcUVn99AkTK1rxxjCd-oU_707N3s23p9OriMaMzCYunuacydj/exec`;

// Put Request setup
let method = "POST";
let headers = {
	"Content-Type": "application/json",
};

/*
 Global window event listener to load DOM when elements are ready
and to then call the createForm function and to hide unessary HTML elements
 */
window.addEventListener("DOMContentLoaded", (e) => {
	// Hide unessary HTML elments
	input.classList.add("display-none");
	cta.classList.add("display-none");
	// Call function to create and display form to the DOM
	createForm();
});

// Function to make a GET request to the API
function loadData(data) {
	// Check if there is a form display div, if there is, remove it.
	let isHere = document.querySelector(".form-display");
	if (isHere) {
		isHere.remove();
	}

	let url = `${baseUrl}?`;
	let value = "";
	let dataD = "";
	for (let input in data) {
		dataD += `${input}=${data[input]}&`;
		value = dataD;
	}

	let tempURL = `${url}${value}`;
	let subUrl = tempURL.slice(0, -1);

	fetch(subUrl)
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			displayFormData(data);
		})
		.catch((error) => {
			console.log(error);
		});
}

// Function to output the form that the user entered in to the DOM
function displayFormData(data) {
	let content = "";
	let keyArray = [];

	Object.entries(data).forEach(([key]) => {
		keyArray.push(key);
	});
	content = `<h1> <u>My Form Data</u></h1>`;
	keyArray.forEach((value) => {
		let dataValue = data[value];
		content += `
                    <h2>${dataValue}</h2>
        
        `;
	});

	// Call function to create and place form element
	let formDisplay = makeHtmlElement(topContent, "div", content, "form-display");
}

// Function that creates and displays the form to the DOM
function createForm() {
	let inputs = "";
	// Create HTML content for the form
	for (let i = 1; i < 11; i++) {
		inputs += `<input placeholder="Enter a value" class="submission-input" type="text" id="field${i}" name="field:${i}" value=""><br>`;
	}
	const selectMenuContent = `
    <form class="submission-form" action="#">
    ${inputs}
    <br><br>
    <input class="submission-btn" type="button" value="Submit Form">
  </form>
`;
	// Call function to create and place form element
	const submissionForm = makeHtmlElement(
		output,
		"div",
		selectMenuContent,
		"submit-form"
	);

	// Function to get form CTA and add an event listener to it.
	createCtaListener(submissionForm);
}

// Function that creates and returns an elmeent
function makeHtmlElement(
	parentElement,
	elementType,
	elementContent,
	elementClass
) {
	const element = document.createElement(elementType);
	element.innerHTML = elementContent;
	element.classList.add(elementClass);
	return parentElement.appendChild(element);
}

// Function that adds event listener to form CTA.
const createCtaListener = (submissionForm) => {
	const formCta = document.querySelector(".submission-btn");
	const formInputs = Array.from(document.querySelectorAll(".submission-input"));

	// Event listener on the form CTA that, when clicked, calls the getFormValues function passing in all the form's inputs as an array
	formCta.addEventListener("click", (e) => {
		// Make constant with a reference to the getFormValues function, passing in the all the form inputs as one combined array
		const formData = getFormValues(formInputs);

		// if the formData object constant is not empty, send it to loadData function to make a fetch request with
		if (Object.keys(formData).length !== 0) {
			loadData(formData);
		} else {
			alert("Please fill out at least one of the form fields");
		}
	});
};

// Function to that receives an array containing all the form inputs
const getFormValues = (formInputs) => {
	let data = {};
	// Loop through the received forInputs array and populate the data object variable with the input name and input value
	formInputs.forEach((input, index) => {
		if (input.value !== "") {
			Object.assign(data, { [index + 1]: `${input.value.trim()}` });
			input.value = "";
		}
	});

	// return data object variable back after its properties' keys/values are dynamically populated with all the form's input names and the correlating input values.
	return data;
};
