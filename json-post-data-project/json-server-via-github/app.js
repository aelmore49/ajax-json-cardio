// GitHub Project

// Elements
const cta = document.querySelector(".btn");
const input = document.querySelector(".input");
const output = document.querySelector(".output");

const url = `https://my-json-server.typicode.com/AXE391/json-data/posts`;

cta.addEventListener("click", (e) => {
	fetch(url)
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			console.log(data);
			displayContent(data);
		});
});

const displayContent = (data) => {
	data.forEach((element) => {
		const { title, status, id } = element;
		output.innerHTML += `
		
		<h1>Name: ${title}</h1>
		<h1>Status: ${status}</h1>
		<h1>Id: ${id}</h1>
		<hr>
		`;
	});
};
