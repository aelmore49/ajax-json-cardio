// API Tester Project using "REQRES.IN"

// Elements
const cta = document.querySelector(".btn");
const input = document.querySelector(".val");
const output = document.querySelector(".output");
const searchDiv = document.querySelector(".search-elements");
// Configure elements
cta.textContent = "Search For User By ID";
input.setAttribute("type", "number");
input.setAttribute("placeholder", "Enter A User Id To Search For");
// Base API URL
const baseUrl = `https://reqres.in/api/users`;
// Object to hold the page data
const pageData = {
	page: 1,
};
// CTA Event Listener
cta.addEventListener("click", () => {
	if (input.value) {
		if (input.classList.contains("invalid-input")) {
			input.classList.remove("invalid-input");
		}
		getUser(input.value);
	} else {
		alert("Please enter in an ID");
		input.classList.add("invalid-input");
	}
});

/*
 Global window event listener to load DOM when elements are ready
and to then call the makeRequest function to send out the inital fetch request to the API
 */
window.addEventListener("DOMContentLoaded", (e) => {
	makeRequest();
});

// Function to make initial Fetch request to the API
function makeRequest(page) {
	if (searchDiv.classList.contains("display-none")) {
		searchDiv.classList.remove("display-none");
	}
	if (input.classList.contains("invalid-input")) {
		input.classList.remove("invalid-input");
	}
	if (page) {
		pageData.page = page;
	}
	const tempUrl = `${baseUrl}?page=${pageData.page}`;
	fetch(tempUrl)
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			displayData(data);
		})
		.catch((error) => {
			console.log(error);
		});
}

// Function to display returned API data to the DOM
function displayData(pageData, type = "", page = "") {
	// Check for type parameter to determine if data is for an indiviual profile page or for a home page
	if (type === "") {
		// Clear any content from output div and setup header
		output.innerHTML = "";
		output.innerHTML = `<div class="output-header"><h2>Page ${pageData.page} of ${pageData.total_pages}</h2></div>`;
		// pull out data object from received data object
		const { data } = pageData;
		// Loop through data object and pull out profile propeties to use for constructing the page
		data.forEach((person) => {
			const { id, email, first_name, last_name, avatar } = person;
			const content = `
        <div data-page=${pageData.page} data-id=${id} class="profile-content">
        <h2 class="name">Name: ${first_name} ${last_name}</h2>
        <h2>Email: ${email}</h2>
        <h2>ID: ${id}</h2>
        <img src="${avatar}" class="profile-pic">
        </div>
        `;
			// Call makeHtmlElement function to make profile div card
			const div1 = makeHtmlElement(output, "div", content, "page-holder");
		});
		// Call function to create pages and pass it the page data
		createPages(pageData);
		// Call function to create click event lister on profile cards
		profileCtaListener();
	}
	// If type parameter is not blank, data is for an indiviual profile page
	else {
		// Call function to create indiviual profile page and pass it the page data and page number
		createIndiviualPage(pageData, page);
	}
}

// Function to make put request
function updateUser(data, id) {
	let updateUrl = `${baseUrl}/${id}`;
	let method = "PUT";
	let headers = {
		"Content-Type": "application/json",
	};
	fetch(updateUrl, {
		method: method,
		headers: headers,
		body: JSON.stringify(data),
	})
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			alert(`Data Updated!: ${JSON.stringify(data)}`);
			makeRequest();
		});
}

// Function that creates and return an elmeent
function makeHtmlElement(
	parentElement,
	elementType,
	elementContent,
	elementClass
) {
	const element = document.createElement(elementType);
	element.innerHTML = elementContent;
	element.classList.add(elementClass);
	return parentElement.appendChild(element);
}

// Funtion to handle clicks on indiviual profile card
function profileHandler(e) {
	// Make constants for id, page number and URL to use for fetch request and to pass to displayData function when called
	const id = e.currentTarget.dataset.id;
	const pageNum = e.currentTarget.dataset.page;
	const profileUrl = `${baseUrl}/${id}`;
	// Make fetch request for indiviual page using the id and profile url above
	fetch(profileUrl)
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			displayData(data, `indiviual`, pageNum);
		})
		.catch((error) => {
			console.log(error);
		});
}

// Function to find a user by their id
function getUser(id) {
	// Get all the profile content divs and put into an array constant called profilePages
	const profilePages = Array.from(
		document.querySelectorAll(".profile-content")
	);
	/*
     Create a constant called hasId and 
     Loop through profilePages and if any of its elements contain the id 
    that was entered in the input field by the user
     */
	const hasId = profilePages.find((page) => {
		return page.dataset.id === id;
	});
	// If hasId is not empty, you it to make a fetch request.
	if (hasId) {
		input.value = "";
		if (input.classList.contains("invalid-input")) {
			input.classList.remove("invalid-input");
		}
		const userId = hasId.dataset.id;
		const pageNum = hasId.dataset.page;
		const userUrl = `${baseUrl}/${userId}`;

		// Make fetch request for indiviual page using the id and profile url above
		fetch(userUrl)
			.then((res) => {
				return res.json();
			})
			.then((data) => {
				displayData(data, `indiviual`, pageNum);
			})
			.catch((error) => {
				console.log(error);
			});
	}
	// If hasId comes back empty, inform user and tell them to try their search again
	else {
		const altPage = +pageData.page == 2 ? "1" : "2";
		alert(
			`No user was found on page ${pageData.page} having an id of ${id}. Please enter in another id or try searching for the id, "${id}", on page ${altPage}.`
		);
		input.classList.add("invalid-input");
		input.value = "";
	}
}

// Function to create pages
function createPages(pageData) {
	// Call makeHtmlElement function to make div holding bottom page pagination CTAs
	const div2 = makeHtmlElement(output, "div", "", "page-holder");
	div2.classList.add("flex-row");
	let button;
	// Loop through all pages and dynamically create page pagination CTAs by calling makeHtmlElement function
	for (let i = 0; i < pageData.total_pages; i++) {
		const content = `${i + 1}`;
		button = makeHtmlElement(div2, "button", content, "page-btn");
		/*  
            Attach a click event listener to each indiviual page pagination CTA, 
            which when clicked, calls the makeRequest function
             and passes it the page number fetch.
              */
		button.addEventListener("click", (e) => {
			window.scroll(0, 0);
			makeRequest(content);
		});
	}
	/* 
        Get all page pagination CTAs and loop through them, placing current page
         styling on the current page pagination CTA.
          */
	const pageCtas = Array.from(document.querySelectorAll(".page-btn"));
	pageCtas.forEach((page) => {
		if (+page.textContent === pageData.page) {
			page.classList.add("current");
		}
	});
}

// Function to create click event listener on profile cards
const profileCtaListener = () => {
	/*
         Get all the profiles and loop through them, adding a click event
         listener to each one that call the profileHandler function when clicked
          */
	const profiles = Array.from(document.querySelectorAll(".profile-content"));
	profiles.forEach((pro) => {
		pro.addEventListener("click", profileHandler);
	});
};

// Function that receives page data and page number and creates indiviual profile pages
const createIndiviualPage = (pageData, page) => {
	// Clear any content from output div and setup header
	output.innerHTML = "";
	// Hide search div
	searchDiv.classList.add("display-none");
	// pull out data object from received data object
	const { data } = pageData;
	// pull out profile data drom pageData object to use for constructing the indiviual profile page
	const { first_name, last_name, email, avatar, id } = data;
	const fullName = `${first_name} ${last_name}`;
	// Setup header for indiviual profile page header
	output.innerHTML = `<div class="output-header"><h2>Name ${fullName} </h2></div>`;
	// Create content for indiviual profile page
	const content = `
<div data-id=${id} data-first-name=${first_name} data-last-name=${last_name} data-email=${email} class="profile-content">
<h2>${fullName} </h2>
<h2 class="email">Email: ${email}</h2>
<h2 class="id">ID: ${id}</h2>
<img class="image" src="${avatar}" class="profile-pic">
</div>
`;
	// Call makeHtmlElement function to make indiviual profile div card
	const profileDiv = makeHtmlElement(
		output,
		"div",
		content,
		"indiviual-profile"
	);
	// Call function to create content for indiviual profile pages
	createProfileContent();
	// Call function to create "Return to Home" CTAs and pass it the page number and the profile div element
	createReturnHomeCta(page, profileDiv);
};

// Function that creates the content for the indiviual profile pages
const createProfileContent = () => {
	const profileContent = document.querySelector(".profile-content");
	const selectMenuContent = `
    <form class="update-profile-form" action="#">
    <label class="update-label" for="udpate">Choose a field to update:</label>
    <select class="update-select" name="udpate" id="update">
      <option value="name">Name</option>
      <option value="email">Email</option>
      <option value="id">Id</option>
    </select>
    <br><br>
    <input class="update-select-btn" type="button" value="Confirm & Edit Profile Field">
  </form>
   `;
	const updateForm = makeHtmlElement(
		profileContent,
		"div",
		selectMenuContent,
		"update-form"
	);
	// Call function to create the "Update Profile" CTA
	createUpdateCTA();
	// Call function that creates Update Profile form CTA and pass it the update form div element
	createFormCta(updateForm);
};

// Function that receives the update form element div and creates the form CTA
const createFormCta = (updateForm) => {
	const updateBtn = document.querySelector(".update-btn");
	const formBtn = document.querySelector(".update-form input");
	formBtn.addEventListener("click", (e) => {
		let selected = document.getElementById("update").value;
		const updateInput = makeHtmlElement(
			updateForm,
			"input",
			"hi",
			"input-update"
		);
		updateInput.setAttribute("placeholder", `${selected} update`);
		updateInput.setAttribute("data-update", `${selected}`);
		formBtn.classList.add("display-none");
		if (updateBtn.classList.contains("display-none")) {
			updateBtn.classList.remove("display-none");
		}
		document
			.querySelector(".update-profile-form")
			.classList.add("display-none");
	});
};

// Function that creates the Update Profile CTA
const createUpdateCTA = () => {
	const profileContent = document.querySelector(".profile-content");
	// // Create and configure Update Profile CTA
	const updateBtnContent = "Update Profile";
	const updateBtn = makeHtmlElement(
		profileContent,
		"button",
		updateBtnContent,
		"update-btn"
	);
	updateBtn.classList.add("display-none");
	// Event Listener for Update Profile CTA
	updateBtn.addEventListener("click", updateAndRequest);
};

// Function that receives the page number, profile div element and then creates the "Return to Home" CTA
const createReturnHomeCta = (page, profileDiv) => {
	// Call makeHtmlElement function to create Return To Home CTA for indiviual profile page
	const btnContent = `Go Back to page ${page}`;
	button = makeHtmlElement(profileDiv, "button", btnContent, "page-btn");
	// Add a click event lister to Return To Home CTA, which calls the makeRequest function passing in the page
	button.addEventListener("click", (e) => {
		makeRequest(page);
	});
};

// Function that reacts to clicks on the Update Profile CTA and then calls the put request method
const updateAndRequest = (e) => {
	const firstName = e.currentTarget.parentElement.dataset.firstName;
	const lastName = e.currentTarget.parentElement.dataset.lastName;
	const fullName = `${firstName} ${lastName}`;
	const id = e.currentTarget.parentElement.dataset.id;
	const email = e.currentTarget.parentElement.dataset.email;
	const inputUpdate = document.querySelector(".input-update");
	const inputUpdateValue = document.querySelector(".input-update").value;
	if (inputUpdateValue !== "") {
		let caseValue = inputUpdate.dataset.update;
		let data = {};
		switch (caseValue) {
			case "email":
				data = {
					name: fullName,
					email: inputUpdateValue,
				};
				updateUser(data, id);
				break;
			case "name":
				console.log("name update request");
				data = {
					name: inputUpdateValue,
					email: email,
				};
				updateUser(data, id);
				break;
			case "id":
				console.log("id update request");
				data = {
					id: inputUpdateValue,
					email: email,
				};
				updateUser(data, id);
				break;
			default:
				console.log("No value found");
		}
	} else {
		alert("Please enter a value in for the profile update");
	}
};
