// JSON Placeholder Project

// Elements
const cta = document.querySelector(".btn");
const output = document.querySelector(".output");

const selectMenu = document.createElement("select");

const confirmCta = document.createElement("button");
const getPostsCta = document.createElement("button");

const selectionDiv = document.createElement("div");
const postContent = document.createElement("div");

postContent.classList.add("post-content");
selectionDiv.classList.add("selection-div");
document.body.append(selectionDiv);
output.parentNode.insertBefore(selectionDiv, output);
confirmCta.textContent = "Confirm";
getPostsCta.textContent = "Get Posts";
cta.textContent = "Submit";

// Create select menus
selectMenu.setAttribute("name", "posts");
selectMenu.setAttribute("id", "posts");

// Select Menu options array
const options = [
	`Select A Post Type`,
	`Posts`,
	`Comments`,
	`Albums`,
	`Photos`,
	`Todos`,
	`Users`,
];

let filterOptions = ["userId", "postId", "albumId", "username"];

options.forEach((option) => {
	let selectOption = document.createElement("option");
	selectOption.value = option.toLowerCase();
	selectOption.innerHTML = option;
	selectMenu.append(selectOption);
});

selectionDiv.append(cta, selectMenu);

cta.parentNode.insertBefore(selectMenu, cta);

// API URL
const baseUrl = `https://jsonplaceholder.typicode.com/`;

cta.addEventListener("click", (e) => {
	const defaultValue = `select a post type`;
	const selectValue = selectMenu.value;
	if (selectValue !== defaultValue) {
		const tempUrl = `${baseUrl}${selectValue}`;
		makeRequest(tempUrl, selectValue);
	} else {
		alert("Please Select A Post Category.");
	}
});

const makeRequest = (tempUrl, selectValue) => {
	fetch(tempUrl)
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			renderData(data, selectValue);
		})
		.catch((error) => {
			console.log(error);
		});
};

const renderData = (data, selectValue) => {
	console.log(data);
	const filterDataSelectMenu = document.createElement("select");
	filterDataSelectMenu.setAttribute("name", "filter");
	filterDataSelectMenu.setAttribute("id", "filter");

	let filterDataSelectArray = ["Get By ID"];
	let selectType = "";

	switch (selectValue) {
		case "comments":
			selectType = "Get By Post Id";
			break;

		case "photos":
			selectType = "Get By Album Id";
			break;

		case "users":
			selectType = "Get By User Name";
			break;
		default:
			selectType = "Get By User Id";
			break;
	}

	filterDataSelectArray.push(selectType);

	filterDataSelectArray.forEach((opt) => {
		let option = document.createElement("option");
		option.value = opt;
		option.innerHTML = opt;
		filterDataSelectMenu.append(option);
	});

	selectionDiv.innerHTML = "";
	selectionDiv.append(confirmCta, filterDataSelectMenu);
	confirmCta.parentNode.insertBefore(filterDataSelectMenu, confirmCta);

	confirmCta.addEventListener("click", (e) => {
		const value = filterDataSelectMenu.value;
		if (value == "Get By ID") {
			selectionDiv.innerHTML = "";
			let universalArray = [];
			let universalSelectMenu = document.createElement("select");
			data.forEach((opt, index) => {
				universalArray.push(index + 1);
			});

			universalArray.forEach((option) => {
				let selectOption = document.createElement("option");
				selectOption.value = option;
				selectOption.innerHTML = option;
				universalSelectMenu.append(selectOption);
			});

			selectionDiv.append(getPostsCta, universalSelectMenu);
			getPostsCta.parentNode.insertBefore(universalSelectMenu, getPostsCta);

			getPostsCta.addEventListener("click", (e) => {
				let content = "";
				const idValue = +universalSelectMenu.value;
				const filterData = data.filter((d) => {
					return d.id === idValue;
				});

				filterData.forEach((data) => {
					content += `
                                            <hr>
                                            <h2 class="${
																							data.name === undefined
																								? "display-none"
																								: ""
																						}">Name: ${data.name} </h2>
            
                                            <h2 class="${
																							data.title === undefined
																								? "display-none"
																								: ""
																						}">Title: ${data.title} </h2>
            
                                            <h2 class="${
																							data.email === undefined
																								? "display-none"
																								: ""
																						}">Email: ${data.email} </h2>
                                                                                        

                                            <h2 class="${
																							data.username === undefined
																								? "display-none"
																								: ""
																						}">User Name: ${data.username} </h2>

            
                                            <h2 class="${
																							data.id === undefined
																								? "display-none"
																								: ""
																						}">Id: ${data.id} </h2>
            
                                            <h2 class="${
																							data.userId === undefined
																								? "display-none"
																								: ""
																						}">User Id: ${data.userId} </h2>
            
                                            <h2 class="${
																							data.postId === undefined
																								? "display-none"
																								: ""
																						}">Post Id: ${data.postId} </h2>
            
                                          <h2 class="${
																						data.albumId === undefined
																							? "display-none"
																							: ""
																					}">Album Id: ${data.albumId} </h2>
            
                                            <h2 class="${
																							data.body === undefined
																								? "display-none"
																								: ""
																						}">Body: ${data.body} </h2>
            
                                                                        <img class="${
																																					data.thumbnailUrl ===
																																					undefined
																																						? "display-none"
																																						: ""
																																				}"  src="${
						data.thumbnailUrl
					}" width="100" height="100">
                                            <hr>
                                        `;
				});
				postContent.innerHTML = content;
				selectionDiv.append(postContent);
			});
		} else {
			selectionDiv.innerHTML = "";
			let universalArray = [];
			let universalSelectMenu = document.createElement("select");
			let keyArray = [];
			let holder = [];

			const dataObj = data[0];

			for (const objKey in dataObj) {
				keyArray.push(objKey);
			}

			let [filterValue] = filterOptions.filter(function (val) {
				return keyArray.indexOf(val) != -1;
			});

			console.log(filterValue);

			data.forEach((obj) => {
				holder.push(obj[filterValue]);
			});

			var uniq = [...new Set(holder)];

			uniq.forEach((opt, index) => {
				universalArray.push(opt);
			});

			uniq.forEach((val) => {
				let option = document.createElement("option");
				option.value = val;
				option.innerHTML = val;
				universalSelectMenu.append(option);
			});

			selectionDiv.append(getPostsCta, universalSelectMenu);
			getPostsCta.parentNode.insertBefore(universalSelectMenu, getPostsCta);

			getPostsCta.addEventListener("click", (e) => {
				let content = "";
				const idValue = universalSelectMenu.value;
				let filterData = data.filter((d) => {
					return d[filterValue].toString() === idValue.toString();
				});

				console.log(filterData);

				filterData.forEach((data) => {
					content += `
				                            <hr>
				                            <h2 class="${
																			data.name === undefined
																				? "display-none"
																				: ""
																		}">Name: ${data.name} </h2>

				                            <h2 class="${
																			data.title === undefined
																				? "display-none"
																				: ""
																		}">Title: ${data.title} </h2>
                                                                        

                                            <h2 class="${
																							data.username === undefined
																								? "display-none"
																								: ""
																						}">User Name: ${data.username} </h2>

				                            <h2 class="${
																			data.email === undefined
																				? "display-none"
																				: ""
																		}">Email: ${data.email} </h2>

				                            <h2 class="${
																			data.id === undefined
																				? "display-none"
																				: ""
																		}">Id: ${data.id} </h2>

				                            <h2 class="${
																			data.userId === undefined
																				? "display-none"
																				: ""
																		}">User Id: ${data.userId} </h2>

				                            <h2 class="${
																			data.postId === undefined
																				? "display-none"
																				: ""
																		}">Post Id: ${data.postId} </h2>

				                          <h2 class="${
																		data.albumId === undefined
																			? "display-none"
																			: ""
																	}">Album Id: ${data.albumId} </h2>

				                            <h2 class="${
																			data.body === undefined
																				? "display-none"
																				: ""
																		}">Body: ${data.body} </h2>

				                                                        <img class="${
																																	data.thumbnailUrl ===
																																	undefined
																																		? "display-none"
																																		: ""
																																}"  src="${
						data.thumbnailUrl
					}" width="100" height="100">
				                            <hr>
				                        `;
				});
				postContent.innerHTML = content;
				selectionDiv.append(postContent);
			});
		}
	});
};
