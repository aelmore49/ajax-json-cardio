"use strict";

// Challenge One : send request to National Park California and Smarty Street APIs using XMLHttpRequest

const url = `https://ajax-practice-elmore.herokuapp.com/nps/api/v1/parks?stateCode=mo`;

const addressKey = `109840332673150815`;
const addressAuthId = `3f15f5e4-137e-8667-464f-139f621a15a2`;
const authToken = `uKdJ9mlzeuJgS09ldJni`;
const addressApiUrl = `https://us-street.api.smartystreets.com/street-address?key=${addressKey}`;

const cta = document.getElementById("submit2");

const userCity = document.querySelector("#city");
const userState = document.querySelector("#state");
const userZip = document.querySelector("#zip");
const userAddress = document.querySelector("#address");

const parkImage = document.querySelector("#specials h2 img");
const parkSection = document.querySelector("#specials");

const parkSpotLightTitle = document.querySelector("#specials h2");
const parkSpotLightLink = document.querySelector("#specials h2 a");
const parkSpotLightPar = document.querySelector("#specials p");

const requestTypes = {
	smarty: `smarty`,
	park: `park`,
};

const smartyInit = {
	headers: {
		"Content-Type": "application/json",
		Host: "us-street.api.smartystreets.com",
	},
};

// Function that is called is request is successful
const updateUISuccess = function (objectData, type = "") {
	if (type === requestTypes.smarty) {
		let zip = `${objectData[0].components.zipcode}-${objectData[0].components.plus4_code}`;
		userZip.value = zip;
	}
	if (type === requestTypes.park) {
		console.log(`This ${type} request was sent successfully:`);
		const random = Math.floor(Math.random() * objectData.data.length);
		const parkName = objectData.data[random].fullName;
		const parkUrl = objectData.data[random].url;
		const parkDescription = objectData.data[random].description;

		parkImage.src = `https://www.nps.gov/common/commonspot/templates/assetsCT/images/branding/logo.png`;
		parkSpotLightLink.href = parkUrl;
		parkSpotLightLink.textContent = `Visit ${parkName}`;
		parkSpotLightPar.textContent = parkDescription;
		parkSection.classList.remove("hidden");
	}
	if (type === "") {
		console.log(`No reqeust type identified:`, objectData);
	}
};
// Function that is called if request erros out
const updateUIError = function (error, type) {
	if (type === requestTypes.smarty) {
		console.log(`this is a smarty request error:${error}`);
	}
	if (type === requestTypes.park) {
		console.log(`this is a park request error:${error}`);
	}
	if (type === "") {
		console.log(`No request type idetified for the error:${error}`);
	}
};

//  --- Below is the way you would send a request using the XHR object ---

// Function that request and receives the api response
// const responseMethod = function (httpRequest, type = "") {
// 	if (httpRequest.readyState === 4) {
// 		if (httpRequest.status === 200) {
// 			updateUISuccess(httpRequest.responseText, type);
// 		} else {
// 			const status = `${httpRequest.status} : ${httpRequest.responseText}`;
// 			updateUIError(status, type);
// 		}
// 	}
// };

// Function that sets the XMLHttpRequest object up and sends the request
// const createRequest = function (url, type) {
// 	const httpRequest = new XMLHttpRequest(url);
// 	httpRequest.addEventListener("readystatechange", (url) => {
// 		responseMethod(httpRequest, type);
// 	});
// 	httpRequest.open("GET", url);
// 	httpRequest.send();
// };

// -- End of XHR request  --

// A function that handles errors for the the below fetch request method
const handleFetchErrors = function (response) {
	console.log("response:", response);
	if (!response.ok) {
		console.log("not good");
		throw new Error(response.status + ": " + response.statusText);
	}
	return response.json();
};

// -- Below is the way you would send a reqeust using the built-in fetch method --
const createRequest = function (url, type) {
	if (type === requestTypes.smarty) {
		fetch(url, smartyInit)
			.then((res) => {
				return handleFetchErrors(res);
			})
			.then((data) => {
				updateUISuccess(data, type);
			})
			.catch((error) => {
				updateUIError(error, type);
			});
	} else {
		fetch(url)
			.then((res) => {
				return handleFetchErrors(res);
			})
			.then((data) => {
				updateUISuccess(data, type);
			})
			.catch((error) => {
				updateUIError(error, type);
			});
	}
};

const checkCompletion = function () {
	if (
		userAddress.value !== "" &&
		userCity.value !== "" &&
		userState.value !== ""
	) {
		const tempUrl = `${addressApiUrl}&street=${userAddress.value}&city=${userCity.value}&state=${userState.value}&zipcode=${userZip.value}&candidates=10`;
		createRequest(tempUrl, requestTypes.smarty);
	}
};

userAddress.addEventListener("blur", checkCompletion);
userCity.addEventListener("blur", checkCompletion);
userState.addEventListener("blur", checkCompletion);

window.addEventListener("DOMContentLoaded", (e) => {
	createRequest(url, requestTypes.park);
});
