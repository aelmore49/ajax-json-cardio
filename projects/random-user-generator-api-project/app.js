// AJAX Random User API Project

// URL
const url = `https://randomuser.me/api/`;

// Page Elements
const btn = document.querySelector(".btn");
const headerTag = document.querySelector("h1");
const outPut = document.querySelector(".output");
const inputValue = document.querySelector(".val");

// Element's Attribute Config
inputValue.setAttribute("type", "number");
btn.textContent = "Click Me";


// Click Handler Function
const clickHandler = () => {
    // Set input value as a default of 10 items
    inputValue.value = (inputValue.value === "") ? 10 : inputValue.value;
    // Have input value determine number of return results
    const tempUrl = `${url}?results=${inputValue.value}`;
    // Setup Request
    fetch(tempUrl)
        .then(res => res.json())
        .then((data) => {
            const results = data.results;
            updateUI(results);
        })
        .catch((e) => {
            console.log(e);
        });
};

// Function to render return API data to the screen
const updateUI = (dataArray) => {
    // loop through array elements and configure content
    dataArray.forEach((user) => {
        const name = `<h2>${user.name.first} ${user.name.last}</h2>`;
        const image = `<img src="${user.picture.large}">`;
        const email = `<h2 class="email">Email:${user.email}</h2>`;
        const info = `<div class="content-container">
        <h2> Gender: ${user.gender}</h2>
        <h2> Age: ${user.dob.age}</h2>
        <h2> Location: ${user.location.state}, ${user.location.country}</h2>
        </div>`;
        const container = `
                            ${name}
                            ${image}
                            ${info}
                            ${email}
                            `;
        // Send profile content to render function to display to UI
        rendUserInfo("div", outPut, container);
    });

};

const rendUserInfo = (element, parent, info) => {
    // Create container div and append to parent.
    const elem = document.createElement(element);
    const selectedElm = document.querySelector(".selected-profile");
    parent.append(elem);
    elem.innerHTML = info;
    elem.classList = "profile-container";
    // Make profile clickable and display at top when clicked
    elem.addEventListener("click", (e) => {
        selectedElm.innerHTML = `${info}`;
        selectedElm.classList.remove("display-none");
        window.scrollTo({ top: 0 });
    });
};

// Button event listener
btn.addEventListener("click", clickHandler);;