// LocalStorage-JSON List Project

// Output data
const output = document.querySelector(".output");

// Button that will reload JSON content to page
const reloadBtn = document.createElement("button");
reloadBtn.textContent = "Reload List";
reloadBtn.addEventListener("click", reloadContent);
document.body.append(reloadBtn);

// Inputs and button that will allow user to add to the list
const inputOne = document.createElement("input");
const inputTwo = document.createElement("input");
const addItemBtn = document.createElement("button");
const addItemContainer = document.createElement("div");
addItemContainer.append(inputOne, inputTwo, addItemBtn);
document.body.append(addItemContainer);
addItemBtn.textContent = "Add List Item";
inputOne.setAttribute('placeholder', 'Name');
inputTwo.setAttribute('type', 'number');
inputTwo.value = '2';
addItemBtn.addEventListener("click", addToList);

// Connect to Json Data
const url = "list.json";

// Make empty list array to store items
let myList = [];

// Set data to list in localStorage
let localData = localStorage.getItem('myList');

// Listen to make sure DOM content has been loaded
window.addEventListener("DOMContentLoaded", () => {
    output.textContent = `Loading...`;

    // Check localStorage for list
    if (localData) {
        myList = JSON.parse(localStorage.getItem("myList"));
        maker();

    } else {
        // Call reload function for JSON content
        reloadContent();
    }

});

// Add item to list function
function addToList() {
    console.log(inputOne.value, inputTwo.value);
    if (inputOne.value !== '' && inputTwo.value !== '') {

        inputOne.classList.remove("required");
        inputTwo.classList.remove("required");

        const myObj = {
            "name": inputOne.value,
            "guests": inputTwo.value,
            "status": false
        };

        const val = myList.length;

        myList.push(myObj);
        saveToStorage();
        makeListItem(myObj, val);
        inputOne.value = '';
    } else {
        if (inputOne.value === '') {
            inputOne.classList.add("required");
        }
        if (inputTwo.value === '') {
            inputTwo.classList.add("required");
        }
        alert(`Please enter in a value for both input fields!`);
        return;
    }

}

// Reload content from JSON file
function reloadContent() {
    // Fetch Request to JSON File
    fetch(url)
        .then(res => res.json())
        .then((data) => {
            myList = data;
            maker();
            saveToStorage();
        });
}

// Populate list array with JSON data
function maker() {
    output.innerHTML = "";
    myList.forEach((el, index) => {
        makeListItem(el, index);
    });
}

// Add list item content to page
function makeListItem(item, index) {
    const content = document.createElement("div");
    content.classList.add("box");
    content.innerHTML =
        `
        <h2>
        Name: ${item.name}
         -- Number of Guests: ${item.guests}
          </h2>
          `;
    output.append(content);

    if (item.status) {
        content.classList.add("confirmed");
    } else if (!item.status) {
        content.classList.add("not-confirmed");
    }

    // Click on the list interaction to change list item status
    content.addEventListener("click", (e) => {
        content.classList.toggle("confirmed");
        content.classList.toggle("not-confirmed");
        if (content.classList.contains("confirmed")) {
            myList[index].status = true;
        } else {
            myList[index].status = false;
        }
        saveToStorage();

    });

    // Span for remove item X button
    const span = document.createElement("span");
    span.textContent = "X";
    content.append(span);
    span.addEventListener("click", (e) => {
        e.stopPropagation();
        content.remove();
        myList.splice(index, 1);
        saveToStorage();

    });
}

// Function to save updates to list to localStorage
function saveToStorage() {
    console.log(myList);
    localStorage.setItem("myList", JSON.stringify(myList));
}