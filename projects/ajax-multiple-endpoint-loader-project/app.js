// Multipe Endpoints API Project

// Endpoints
const urls = [
    {
        'url': `https://www.discoveryvip.com/shared/books2.json`,
        'arr': `books`,
        'title': 'Books List'
    },
    {
        'url': `https://www.discoveryvip.com/shared/people.json`,
        'arr': `people`,
        'title': 'People List'
    },
    {
        'url': `https://www.discoveryvip.com/shared/coin.json`,
        'arr': `data`,
        'title': 'BitCoin Currency'
    }
];

// Elements
const output = document.querySelector(".output");
const inputVal1 = document.querySelector(".val");
const btnHolder = document.querySelector(".btn-holder");


// Loop through all urls and give them all clickable buttons
urls.forEach((url) => {
    // Setup url's buttons
    const urlBtn = document.createElement("button");
    // Set button content to url title
    urlBtn.textContent = `Select ${url.title}`;
    urlBtn.classList = "url-button";

    // Append button to h1
    btnHolder.append(urlBtn);

    // Add event listener to button which sends url object for Api request
    urlBtn.addEventListener("click", (e) => {
        sendRequest(url);
    });
});



// Function that receives URLs and sends out request
const sendRequest = (obj) => {

    let url = obj.url;

    //setup fetch request to endpoint
    fetch(url)
        .then((res) => res.text())
        .then((data) => {

            // Convert returned data into a javascript object
            const myData = JSON.parse(data);

            // Display endpoint
            output.innerHTML = `<h2>Endpoint:</h2> <h3>${url}</h3> <br>`;

            // send data from data to ui updater function
            updateUI(myData[obj.arr]);
        })
        .catch((err) => {
            console.log(err);
        });
};

// Function to display content to UI
const updateUI = (arr) => {


    // Loop through array
    arr.forEach((el) => {

        // Create UI elements
        const contentContainer = document.createElement("div");
        contentContainer.classList = "content-container";
        output.append(contentContainer);

        // Get object keys and values and loop through them
        const entries = Object.entries(el);
        contentContainer.innerHTML = `<h2>Properties :</h2> <h3>${entries.length}</h3>`;
        for (const [key, value] of entries) {
            // Set content to key/value of each object 
            contentContainer.innerHTML += `<br><h2>${key} :</h2> <h3>${value} </h3><br>`;
        }

    });
};
