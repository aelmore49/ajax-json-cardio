// Starwars API Project

// Base URL
const url = `https://swapi.dev/api/`;

// Search URL
const searchURL = `https://swapi.dev/api/people/?search=`;

// Element
const btn = document.querySelector(".btn");
btn.textContent = "Search";
btn.classList.add("search-btn");
const outPut = document.querySelector(".output");
const inputValue = document.querySelector(".val");
const buttonHolder = document.querySelector(".button-holder");

inputValue.setAttribute("placeholder", "Search Characters");

// Window event listener
window.addEventListener("DOMContentLoaded", (e) => {
    console.log("DOM ready");
    // Seupt fetch request to the main URL
    fetch(url)
        .then((res) => res.json())
        .then((data) => {
            for (const key in data) {
                // Call Render Content Method with object key and property
                renderContent(`${data[key]}`, `${key}`);
            }
        });
});

const renderContent = (urlName, name) => {
    // Create button elements for indiviual items in the object
    const button = document.createElement("button");
    // Configure button element's attributes and properties
    button.classList = "button-items";
    button.setAttribute("data-url", `${urlName}`);
    button.setAttribute("data-type", `${name}`);
    button.textContent += `${name}`;
    // Append button elements to button holder div
    buttonHolder.append(button);
    // Set an event listener for each button element
    button.addEventListener("click", buttonItemsHandler);
};

// Function to handle click event on button items
const buttonItemsHandler = (e) => {
    // Clear content for outPut div and remove display none class
    outPut.innerHTML = "";
    outPut.classList.remove("display-none");
    // Configure attributes
    const requestURL = e.target.getAttribute("data-url");
    const nextURL = e.target.getAttribute("data-next-url");
    const prevURL = e.target.getAttribute("data-prev-url");
    // If requestURL data attribute, make fetch request URL
    if (requestURL) {
        fetch(requestURL)
            .then((res) => res.json())
            .then((data) => {
                renderButtonContent(data.results, e.target, data);
            });
    }

    // If next page URL data attribute, make fetch to next page URL
    else if (!requestURL && nextURL) {
        fetch(nextURL)
            .then((res) => res.json())
            .then((data) => {
                renderButtonContent(data.results, e.target, data);
            });
    }

    // If previous page URL data attribute, make fetch to previous page URL
    else if (!requestURL && prevURL) {
        fetch(prevURL)
            .then((res) => res.json())
            .then((data) => {
                renderButtonContent(data.results, e.target, data);
            });
    }
};

// Funtion to render clicked anchor content
const renderButtonContent = (data, target, resultObject) => {
    const name = target.getAttribute("data-type");
    switch (name) {
        // If films data, config for it.
        case "films":
            data.forEach((result) => {
                const elButton = document.createElement("button");
                elButton.textContent = `${result.title}`;
                elButton.classList.add("box");
                elButton.url = `${result.url}`;
                elButton.addEventListener("click", showButtonContent);
                outPut.append(elButton);
            });
            break;
        default:
            // if anything other than films data, config accordingly
            data.forEach((result) => {
                const elButton = document.createElement("button");
                elButton.textContent = `${result.name}`;
                elButton.classList.add("box");
                elButton.url = `${result.url}`;
                elButton.addEventListener("click", showButtonContent);
                outPut.append(elButton);
            });
            break;
    }

    // Setup and configure for Next and Previous Page Buttons
    const pages = document.createElement("div");
    pages.classList.add("pages-container");
    outPut.append(pages);
    if (resultObject.next) {
        const nextPageBtn = document.createElement("button");
        nextPageBtn.textContent = "View Next Page";
        nextPageBtn.classList.add("box");
        nextPageBtn.classList.add("next");
        pages.append(nextPageBtn);
        nextPageBtn.addEventListener("click", (e) => {
            e.target.setAttribute("data-next-url", `${resultObject.next}`);
            buttonItemsHandler(e);
        });
    }

    if (resultObject.previous) {
        const prevPageBtn = document.createElement("button");
        prevPageBtn.textContent = "View Previous Page";
        prevPageBtn.classList.add("box");
        prevPageBtn.classList.add("next");
        pages.append(prevPageBtn);
        prevPageBtn.addEventListener("click", (e) => {
            e.target.setAttribute("data-prev-url", `${resultObject.previous}`);
            buttonItemsHandler(e);
        });
    }
};

// Funtion to show content in indiviual button items
const showButtonContent = (e) => {
    const el = e.target;
    const elUrl =
        el.url !== undefined ? el.url : e.target.getAttribute("data-url");
    outPut.innerHTML = "";
    outPut.innerHTML = `<h2 class="output-header">In a galaxy far, far away...<h2>`;

    // Set and configure Back Page Button
    const backButton = document.createElement("button");
    backButton.textContent = "Home";
    backButton.classList.add("box");
    backButton.classList.add("next");
    backButton.addEventListener("click", (e) => {
        outPut.innerHTML = "";
        const parentUrl = elUrl.split("/", 5).join("/");
        fetch(parentUrl)
            .then((res) => res.json())
            .then((data) => {
                renderButtonContent(data.results, e.target, data);
            });
    });

    // Make fetch request to button item url
    fetch(elUrl)
        .then((res) => res.json())
        .then((data) => {
            for (const key in data) {
                let html =
                    typeof data[key] === "string" ? data[key] : JSON.stringify(data[key]);
                outPut.innerHTML += `<div class="content-container">
                                    <span class="key">${key}</span> : ${html}
                                    </div>`;
            }
            outPut.append(backButton);
        })
        .catch((e) => {
            outPut.innerHTML = "Error";
        });
};

// Search Button event listener 
// TODO -- Make event listener call seperate function
btn.addEventListener("click", (e) => {
    // Check for input value for search
    if (`${inputValue.value}` !== "") {
        outPut.innerHTML = "";
        const tempURL = `${searchURL}${inputValue.value}`;
        fetch(tempURL)
            .then((res) => res.json())
            .then((data) => {
                if (data.results.length >= 1) {
                    const resultURL = data.results[0].url;
                    e.target.setAttribute("data-url", `${resultURL}`);
                    fetch(resultURL)
                        .then((res) => res.json())
                        .then((data) => {
                            outPut.innerHTML += ` <h3 class="content-container"> Results for "${inputValue.value}".</h3>`;
                            for (const key in data) {
                                let html =
                                    typeof data[key] === "string"
                                        ? data[key]
                                        : JSON.stringify(data[key]);
                                outPut.innerHTML += `<div class="content-container">
                                                    <span class="key">${key}</span> : ${html}
                                                    </div>`;
                                outPut.classList.remove("display-none");
                                inputValue.value = "";
                            }
                        });
                } else if (data.results.length <= 0) {
                    const noResults = document.createElement("div");
                    noResults.classList.add("content-container");
                    noResults.innerHTML = `<h3> No results for "${inputValue.value}" were found.</h3>`;
                    outPut.classList.remove("display-none");
                    outPut.append(noResults);
                }
            });
    } else {
        alert("Please enter a search term");
    }
});
