// Country Data API Project

// API Base URL
const baseURL = `https://restcountries.eu/rest/v2/all`;

// Elements
const btn = document.querySelector(".btn");
const inputValue = document.querySelector(".val");
let outPut = document.querySelector(".output");
const searchUrl = `https://restcountries.eu/rest/v2/name/`;
const topOutput = document.querySelector(".top-output");

//Global page object
const pageObject = { json: {}, start: 0, itemsPerPage: 10, arr: [] };

// Configure Element properties and attributes
btn.textContent = "Search By Name";
inputValue.setAttribute("placeholder", "Country Name");
inputValue.setAttribute("type", "text");

const pageIndex = document.createElement("h2");
pageIndex.classList.add("page-index");

const goToTheTop = document.createElement("button");
goToTheTop.textContent = "Go To The Top";
goToTheTop.classList.add("go-to-top");
goToTheTop.classList.add("btn");

const buttomButtonHolder = document.createElement("div");
buttomButtonHolder.classList.add("bottom-button-holder");
outPut.parentElement.insertBefore(
	buttomButtonHolder,
	outPut.nextElementSibling
);

// Global window event listener to load DOM when elements ready
window.addEventListener("DOMContentLoaded", (e) => {
	console.log("DOM Ready!");
	buttomButtonHolder.append(goToTheTop);

	const dataForCountries = JSON.parse(localStorage.getItem("countryData"));
	if (dataForCountries) {
		console.log("No Fetch required");
		createPages(dataForCountries);
		createPageBtns(dataForCountries);
	} else if (!dataForCountries) {
		console.log("Need data. Fetch requred.");
		// After elements have been loaded to the DOM, make initial fetch request to baseURL
		fetch(baseURL)
			.then((res) => {
				return res.json();
			})
			.then((data) => {
				localStorage.setItem("countryData", JSON.stringify(data));
				createPages(data);
				createPageBtns(data);
			});
	}
});

// Function to render countries to the page
const renderCountries = (index, data) => {
	// Create current and total page constants
	const tltPageAmt = pageObject.arr.length.toString();
	const curPage = index === undefined ? 1 : index + 1;
	pageIndex.innerHTML = "";

	// Set active state to current pagination button

	const countryBtns = Array.from(document.querySelectorAll(".country-btn"));
	countryBtns.forEach((btn) => {
		const id = btn.getAttribute("btn-id");
		if (id === curPage.toString()) {
			btn.classList.add("active");
		} else {
			btn.classList.remove("active");
		}
	});

	// Use current and total page constants to create and display page index to the DOM
	pageIndex.textContent = `Page ${curPage} of ${tltPageAmt}`;
	topOutput.append(pageIndex);

	inputValue.value = "";
	outPut.innerHTML = "";

	let start = index ? index : pageObject.start;
	let dataArray = data ? data : pageObject.arr[start];
	dataArray.forEach((country, index) => {
		const { name, capital, population, region, flag, currencies, languages } =
			country;
		const [langName] = languages;
		const [currName] = currencies;
		const countryHolder = document.createElement("div");
		countryHolder.classList.add("indiviual-country");
		outPut.append(countryHolder);

		const countryInfo = document.createElement("div");
		countryInfo.classList.add("country-info");
		countryInfo.innerHTML = `
								<img src="${flag}" alt="${name}'s flag" class="country-flag">
								<h2> Name: ${name}</h2>
								<h2> <i>Click for more information</i></h2>
								 `;
		countryHolder.append(countryInfo);

		const additionalInfo = document.createElement("div");
		additionalInfo.classList.add("additional-info");
		additionalInfo.classList.add("display-none");

		additionalInfo.innerHTML = `
								<h2> Capital: ${capital}<h2>
								<h2> Pop: ${population}<h2>
								<h2> Region: ${region}<h2>
								<h2> Currencies: ${currName.name}<h2>
								 <h2> Languages: ${langName.name}<h2>
								 <h2> <i>Click to go back to Country's Name & Flag </i></h2>
	
								 `;
		countryHolder.append(additionalInfo);

		countryHolder.addEventListener("click", (e) => {
			if (
				!countryInfo.classList.contains("display-none") &&
				additionalInfo.classList.contains("display-none")
			) {
				countryInfo.classList.add("display-none");
				additionalInfo.classList.remove("display-none");
			} else {
				countryInfo.classList.remove("display-none");
				additionalInfo.classList.add("display-none");
			}
		});
	});

	goToTheTop.addEventListener("click", (e) => {
		window.scrollTo({ top: 0 });
	});
};

// Function for country search
const searchCountry = () => {
	if (inputValue.value !== "") {
		const countryName = inputValue.value;
		const [...page] = pageObject.arr;
		let foundNames = [];
		page.forEach((country) => {
			country.forEach((name) => {
				name.name.split(" ").find((cName) => {
					if (cName.toLowerCase() === countryName.toLowerCase()) {
						foundNames.push(name);
					}
				});
			});
		});
		if (foundNames.length === 0) {
			alert("No results found.");
			inputValue.value = "";
		} else {
			renderCountries(0, foundNames);
		}
	} else {
		alert("Please entry the name of a country to search for.");
	}
};

// function to create page that holds ten countries per page.
const createPages = (data) => {
	pageObject.arr.length = 0;
	for (let i = 0; i < data.length; i += pageObject.itemsPerPage) {
		let tempArr = data.slice(i, i + pageObject.itemsPerPage);
		pageObject.arr.push(tempArr);
	}
	renderCountries();
};

// Function for create country pagination buttons.
const createPageBtns = (data) => {
	pageObject.arr.forEach((btn, index) => {
		const countryBtn = document.createElement("button");
		countryBtn.classList.add("btn");
		countryBtn.classList.add("country-btn");
		countryBtn.textContent = index + 1;
		countryBtn.setAttribute("btn-id", (index + 1).toString());
		buttomButtonHolder.append(countryBtn);
		countryBtn.addEventListener("click", (e) => {
			renderCountries(index);
		});
	});
};

// Button event listener for search
btn.addEventListener("click", searchCountry);
