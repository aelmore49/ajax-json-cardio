// Stackexchange API Project

// API base URL
const baseURL = `https://api.stackexchange.com`;

// Elements
const pageButton = document.querySelector(".btn");
const outPut = document.querySelector(".output");
const input = document.querySelector(".val");

// Configure Elements
input.setAttribute("placeholder", "Search for Questions");
input.setAttribute("type", "text");
pageButton.textContent = "Search";

// Window Event Listener to ensure that the content has been loaded to the DOM
window.addEventListener("DOMContentLoaded", (e) => {
	console.log("DOM Ready!");

	// Function to send fetch request to API
	pageLoad();
});

// Function for that handles clicks event on pageButton
const pageButtonHandler = (e) => {
	if (input.value !== "") {
		let tempURL = `${baseURL}/2.3/search?order=desc&sort=activity&intitle=${input.value}&site=stackoverflow`;

		fetch(tempURL)
			.then((res) => {
				return res.json();
			})
			.then((data) => {
				console.log(data);
				// Pull items array out of returned data

				const { items } = data;
				// Function to output items

				outputItems(items);
			})
			.catch((error) => {
				console.log(error);
			});
	} else if (input.value === "") {
		alert("Enter a search term to search for questions.");
	}
};

// Function make request to API once content is loaded to the DOM.
const pageLoad = (e) => {
	// Configure baseURL
	const url = `${baseURL}/2.3/questions?order=desc&sort=activity&site=stackoverflow`;

	// Make fetch request to the API
	fetch(url)
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			// Pull items array out of returned data
			const { items } = data;

			// Function to output items
			outputItems(items);
		})
		.catch((error) => {
			console.log(error);
		});
};

// Function to output items
const outputItems = (items) => {
	// Clear ouput div's content
	outPut.innerHTML = "";
	if (items.length) {
		if (input.value !== "") {
			const searchResults = `There are ${items.length} results for "${input.value}."`;
			makeNode(outPut, "h3", searchResults);
		}

		items.forEach((item) => {
			outputItemtoPage(item);
		});
	} else if (!items.length) {
		// Function that gets called if there are no results for search term
		noResultsFound();
	}
};

// Function to output items to the page
const outputItemtoPage = (item) => {
	const elementContainer = makeNode(outPut, "div", "");
	elementContainer.classList.add("element-container");
	console.log("This is the item:", item);
	if (item.is_answered) {
		elementContainer.style.border = "thick solid gold";
	}
	elementContainer.addEventListener("click", (e) => {
		const acceptedAnswerId = item.accepted_answer_id;
		const question_id = item.question_id;
		const getAnswer = `https://stackoverflow.com/a/${acceptedAnswerId}`;
		const getQuestion = `${baseURL}/2.3/questions/${question_id}?order=desc&sort=activity&site=stackoverflow`;

		if (item.is_answered) {
			window.open(getAnswer);
		} else {
			window.open(getQuestion);
		}
	});

	// Element that we want appended to the DOM
	const element = makeNode(elementContainer, "h3", item.title);
	const { question_id } = item;
	element.setAttribute("data-id", question_id);

	const elmentTagContainer = makeNode(elementContainer, "ul", "");
	elmentTagContainer.classList.add("tag-container");

	const elmentTagTitle = makeNode(elmentTagContainer, "h4", "Tags");
	elmentTagTitle.classList.add("tag-title");

	const elementTags = item.tags.forEach((tag) => {
		const tags = makeNode(elmentTagContainer, "li", tag);
		tags.classList.add("list-item");
	});

	elementContainer.addEventListener("click", getQuestionInfo);
};

// Function to output items to the page
const displaySearch = (item) => {
	const searchResultsHeader = makeNode(outPut, "h3", `${item}`);
	searchResultsHeader.classList.add("search-findings");
};

// Make elements for the page
const makeNode = (parent, elementType, htmlContent) => {
	const element = document.createElement(`${elementType}`);
	element.innerHTML = htmlContent;
	parent.append(element);
	return element;
};

// Function that display a message informing user that their entered search term produced not results
const noResultsFound = () => {
	const noResults = makeNode(
		outPut,
		"h3",
		`no results found for "${input.value}"`
	);
};

const getQuestionInfo = (e) => {
	const id = e.currentTarget.querySelector("h3").dataset.id;
	const tempURL = `${baseURL}/2.3/questions/${id}/answers?order=desc&sort=activity&site=stackoverflow`;

	fetch(tempURL)
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			console.log(data);
			if (data.items.length > 0) {
				console.log(data);
			} else {
				console.log("no answers to the your question");
			}
		});
};

pageButton.addEventListener("click", pageButtonHandler);
