// AJAX WikiMap API Project

// URL for Request
let url = `http://api.wikimapia.org/?key=example&function=place.getnearest`;

// format parameter
const format = 'json';

// Elements
const btn = document.querySelector('.btn');
const output = document.querySelector('.output');
const inputVal = document.querySelector('.val-1');
const inputValTwo = document.querySelector(".val-2");

// Element Properties Config
inputVal.setAttribute("placeholder", "Enter Latitude");
inputValTwo.setAttribute("placeholder", "Enter Longitude");
btn.textContent = "Search Cordinates";

// Search Handler function
const searchResultsHandler = (e) => {

    // Set default values for latitude and longitude
    const latitude = inputVal.value = inputVal.value === '' ? '48.858252' : inputVal.value;
    const longitude = inputValTwo.value = inputValTwo.value === '' ? '2.29451' : inputValTwo.value;

    // Configure URL with Lat and Long values
    const tempUrl = url += `&lat=${latitude}&lon=${longitude}&format=${format}`;

    // Fetch Request to URL
    fetch(tempUrl)
        .then((res) => res.json())
        .then((data) => {

            const stringData = JSON.stringify(data);
            // Call method to update UI, passing stringified data returned from API
            updateUI(stringData);
        })
        .catch((e) => console.log(e));
};

// Method to update UI with data return back from Wiki Maps API
const updateUI = (data) => {
    // Parse received data and use select places array out of it.
    const obj = JSON.parse(data);
    const places = obj.places;

    // Loop through places array
    places.forEach((place) => {

        //pull out URL and title from places array
        const locationUrl = place.url;
        const locationTitle = place.title;

        // Setup anchor tag 
        const anchorTag = document.createElement("a");
        anchorTag.setAttribute("href", locationUrl);
        anchorTag.setAttribute("target", "_blank");

        // Setup content container div
        const locationContainer = document.createElement("div");

        // Setup header title tag
        const headerTag = document.createElement("h1");
        headerTag.textContent = locationTitle;

        // Append elements
        anchorTag.append(headerTag);
        locationContainer.append(anchorTag);
        output.append(locationContainer);
    });

};

// Event Handler for button
btn.addEventListener('click', searchResultsHandler);