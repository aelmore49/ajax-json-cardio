// Simple Ajax to JSON Endpoint

// Endpoint URL
const url = `https://www.discoveryvip.com/shared/test1.json`;

// Select Button object from page and add event listener to it.
const btn = document.querySelector(".btn");
btn.textContent = "Load JSON Data";

// Setup area within HTML for output
const output = document.querySelector(".output");

// Hide input element
const inputVal = document.querySelector(".val");
inputVal.style.display = "none";

// Event listeners
btn.addEventListener("click", loadContent);

// Method for fetch request to Endpoint URL
function loadContent() {
    fetch(url)
        .then((res) => res.json())
        .then((data) => {
            maker(data);
        })
        .catch(err => {
            console.log(err);
        });
}

// Method to loop through data array and output it to DOM
function maker(data) {
    output.innerHTML = `<h1> JSON Data </h1>`;
    data.forEach((el, index) => {
        const bg = index % 2 == 0 ? '#ccc' : '#fff';
        const info = document.createElement("div");
        info.style.backgroundColor = bg;
        info.style.padding = "5px";
        info.innerHTML += `<h2>Name: ${el.name.first} ${el.name.last}</h2>`;
        info.innerHTML += `<h2>Age: ${el.age}</h2>`;
        info.innerHTML += `<h2>Location: ${el.location.city}, ${el.location.country}</h2>`;
        output.append(info);
    });
};