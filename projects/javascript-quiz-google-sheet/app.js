/// Dynamic JavaScript Quiz Project Using Google Sheets

// The Google sheet file id.
const id = "1nGGk-pBhzZd1Hn-T1UhZD5ypTyiI-09-akxwXJ0--1s";

// Google sheet URL for JSON data
const url =
	"https://spreadsheets.goolge.com/feeds/list/" +
	id +
	"/1/public/values?alt=json";

console.log(url);

// Elements
const output = document.querySelector(".output");

const btn = document.querySelector(".btn");
btn.textContent = "Start Game";

const nextBtn = document.createElement("button");
nextBtn.textContent = "Next Question";
nextBtn.classList.add("btn");
nextBtn.classList.add("next-btn");

const newGame = document.createElement("button");
newGame.textContent = "Start New Game?";
newGame.classList.add("btn");

const playerStats = document.createElement("div");
playerStats.classList.add("player-stats");

let playerScore = document.createElement("div");
playerScore.classList.add("player-score");

let questionHolder = document.createElement("div");

let answerHolder = document.createElement("div");

// Global variables and constants.
let questions = [];

let current = 0;

const player = {
	score: 0,
	answers: [],
	wrongAnswers: [],
};

// Global window object event listener to load the content on page load.
window.addEventListener("DOMContentLoaded", () => {
	console.log(`DOM is ready!`);
	// Make fetch request to json file.
	fetch(url)
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			questions = data;
		});
});

// Event Listener for Start Game button.
btn.addEventListener("click", (e) => {
	if (current < questions.length) {
		btn.classList.remove("start-btn");
		btn.style.display = "none";
		loadQuestion();
	}
});

// function to load question.
const loadQuestion = () => {
	// Clear out any content for output div.
	output.innerHTML = "";

	// Constants to keep track of number of total questions, current question number
	// and current question output.
	const numberOfQuestions = questions.length;
	const currentQuestion = current + 1;
	const curQueOutput = questions[current].question;

	// Array variable to push and hold answers.
	let answers = [];

	// Set playerScore and questionHolder divs' content using constants listed above.
	playerScore.innerHTML = `
    <h1>Question #${currentQuestion} of ${numberOfQuestions}
        -- Score: ${player.score}</h1>

    `;
	questionHolder.innerHTML = `<h2 class="current-question">${curQueOutput}</h2>`;

	// Add incorrect answers to answer array.
	questions[current].incorrect.forEach((q) => {
		answers.push(q);
	});

	// Add correct answers to answer array.
	answers.push(questions[current].correct);

	// Suffle array items so correct answer is in a different spot each time.
	answers.sort(() => Math.random() - 0.5);

	// append playerScore and questionHolder divs to the output div.
	output.append(playerScore);
	output.append(questionHolder);

	// Create data attribute holding the correct answer to its respected question.
	output.setAttribute("data-answer", `${questions[current].correct}`);

	// Loop through all items in answer array and turn them into clickable button elements.
	answers.forEach((a) => {
		const answerBtn = document.createElement("button");
		answerBtn.classList.add("answer-btn");
		answerBtn.textContent = a;

		// Apend answer buttons to output div and setup event listener for answer buttons.
		output.append(answerBtn);
		answerBtn.addEventListener("click", checkAnswer);
	});
};
// Function to check if user selection is a correct or incorrect answer to the displayed question.
const checkAnswer = (e) => {
	// Setup constant for correct answer, last question and current question.
	const correctAnswer = e.target.parentElement.getAttribute("data-answer");
	const lastCorrectAnswer = questions.at(-1).correct;
	const currentQuestion =
		document.querySelector(".current-question").textContent;

	// check user answer button click to see if it is correct and or incorrect.
	if (e.currentTarget.textContent === correctAnswer) {
		// If correct, make button green, add to player.score object, notify user and push correct question to player.answers array.
		e.currentTarget.style.background = "green";
		player.score++;
		alert("That is corret! Nice job!");
		player.answers.push(currentQuestion);
	} else {
		// If incorrect, make button red, notify user and push incorrect question to player.wrongAnswers array.
		e.currentTarget.style.background = "red";
		alert(`Sorry. That is incorret. The correct answer is "${correctAnswer}."`);
		player.wrongAnswers.push(currentQuestion);
	}
	// Disable all answer buttons to prevent user from clicking on more answer buttons.
	document.querySelectorAll(".answer-btn").forEach((btn) => {
		btn.disabled = true;
		btn.style.opacity = "0.5";
		btn.style.cursor = "not-allowed";
	});
	// Check to see if current question is the last question.
	// If current question is not last question, display nextBtn and prepare to load another question.
	if (correctAnswer !== lastCorrectAnswer) {
		output.append(nextBtn);
		nextBtn.addEventListener("click", (e) => {
			loadQuestion();
		});
	} else {
		// If current question is the last question, clear output div's content and setup playerStats div's content.
		output.innerHTML = "";
		playerStats.innerHTML = `
				<h2> <u> Player Stats:</u></h2>
				<h2> Results: ${player.answers.length} correct answers out of ${
			questions.length
		} questions</h2>
				<h2> Score: ${player.score} <h2>
				<h2> Questions Answered Correctly:</h2>
				<ul>
					${player.answers
						.map(
							(elmt) => `
						<li>${elmt}</li>
					`
						)
						.join("")}
				</ul>
				<h2> Questions Missed:<h2>
				<ul>
					${player.wrongAnswers
						.map(
							(elmt) => `
						<li>${elmt}</li>
					`
						)
						.join("")}
				</ul>
			`;

		// Append playerStats div to output div and then display newGame button.
		output.append(playerStats);
		newGame.style.display = "inline-block";
		output.append(newGame);
		newGame.addEventListener("click", startNewGame);
	}
	// Increment current count.
	current++;
};

// Function to start a new game.
const startNewGame = () => {
	// Reset all variables, hide newGame button and load questions for the new game.
	current = 0;
	player.score = 0;
	player.answers.length = 0;
	player.wrongAnswers.length = 0;
	newGame.style.display = "none";
	loadQuestion();
};
