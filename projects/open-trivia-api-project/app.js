// Open Trivia Database API Project

// Base API URL
const baseURL = `https://opentdb.com/api.php?`;

// Category API URL
const catURL = `https://opentdb.com/api_category.php`;

// Setup paramater for category selection.
let catID = `&category=`;

// Setup paramater for question difficulty
let difficulty = `&difficulty=`;

// Elements
const btn = document.querySelector(".btn");
const outPut = document.querySelector(".output");
const input = document.querySelector(".val");
const h3 = document.querySelector("h3");
const h1 = document.querySelector("h1");
const categoryInput = document.createElement("select");
const difficultyInput = document.createElement("select");
input.setAttribute("placeholder", "Amount of Questions");

// Add CSS classes to necessary elements
input.classList.add("inputs");
categoryInput.classList.add("inputs");
difficultyInput.classList.add("inputs");
h3.classList.add("question-header");

// Global Game Object
const game = {
	questions: [],
	question: 0,
	difficulty: ["Any Difficulty", "Easy", "Medium", "Hard"],
	correct: 0,
	incorrect: 0,
	elements: [],
	category: "",
	level: "",
};

// Widnow Event Listener to ensure content is loaded to the DOM
window.addEventListener("DOMContentLoaded", (e) => {
	console.log("DOM is ready");
	// Element Config
	btn.textContent = "Start Game";
	btn.classList.add("btn-1");
	btn.classList.add("start-game");
	input.setAttribute("type", "number");

	// Make fetch call for category options
	fetch(catURL)
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			const { trivia_categories } = data;
			trivia_categories.forEach((cat, index) => {
				const option = document.createElement("option");
				option.textContent = cat.name;
				option.setAttribute("data-id", `${cat.id}`);
				option.setAttribute("data-name", `${cat.name}`);
				categoryInput.append(option);
				input.parentNode.insertBefore(categoryInput, input.nextSibling);
			});
			// Call function once an option from the Category Select menu has been selected by the user
			categoryInput.addEventListener("change", setCategoryMenu);

			// Create and populate Difficulty Select menu with difficulty array values
			game.difficulty.forEach((diff) => {
				const option = document.createElement("option");
				option.textContent = diff;
				option.setAttribute("data-name", `${diff}`);
				difficultyInput.append(option);
				// Once Category Menu is loaded, insert Difficulty menu next to it.
				if (categoryInput) {
					categoryInput.parentNode.insertBefore(
						difficultyInput,
						categoryInput.nextSibling
					);
				}
			});

			// Call function once an option from the Difficulty Select menu has been selected by the user
			difficultyInput.addEventListener("change", setDifficulty);
		});
});

// Function that handles user Category Menu selection
const setCategoryMenu = (e) => {
	const id = e.target.options[e.target.selectedIndex].dataset.id;
	const name = e.target.options[e.target.selectedIndex].dataset.name;

	// Set game.category to the selected option's name dataset attribute
	game.category = name;

	// Clear default category pick
	if (catID !== "&category=") {
		catID = "&category=";
	}
	// Set category id to the selected option's id
	catID += id;
};

// Function that handles user Difficulty Menu selection
const setDifficulty = (e) => {
	const value = e.target.value.toLowerCase();
	let selectedDiff = value === "any difficulty" ? "" : value;

	// Set game.difficulty to the selected option's name dataset attribute
	const name = e.target.options[e.target.selectedIndex].dataset.name;
	game.level = name;

	// clear default difficulty pick
	if (difficulty !== "&difficulty=") {
		difficulty = "&difficulty=";
	}
	// Set difficulty to the selected option's value
	difficulty += selectedDiff;
};

// Function for Button Click Event Listener
const btnHandler = (e) => {
	// Setup and configure URL going to API with user selected options
	let tempURL = `${baseURL}amount=${input.value}`;

	if (catID === "&category=") {
		catID = "";
	}
	if (difficulty === "&difficulty=") {
		difficulty = "";
	}

	if (difficulty === "" && catID === "") {
		tempURL = tempURL;
	} else if (difficulty !== "" && catID === "") {
		tempURL = `${tempURL}${difficulty}`;
	} else if (difficulty === "" && catID !== "") {
		tempURL = `${tempURL}${catID}`;
	} else if (difficulty !== "" && catID !== "") {
		tempURL = `${tempURL}${catID}${difficulty}`;
	}

	// Validate value entered in by user
	if (input.value !== "" && input.value > 0) {
		// Hide button and input elements and launch into the game
		btn.classList.add("display-none");
		btn.classList.remove("start-game");
		input.classList.add("display-none");
		categoryInput.classList.add("display-none");
		difficultyInput.classList.add("display-none");
		outPut.classList.remove("display-none");
		// Updated questions header and append to output div
		h3.textContent = `${input.value} question(s) selected`;

		// Call Function the makes request to the API
		getContent(tempURL);
	} else {
		// Alert user to enter a non negative or 0 based number
		alert(
			"Please enter a number grater than 0 for how many questions you want for your game."
		);
		input.value = "";
	}
};

// Function to move user to the next question
const nextQuestion = (parent) => {
	const nextButton = displayContent(parent, "button", "Next Question");
	nextButton.classList.add("btn-1");
	nextButton.classList.add("next-question");

	// If next button gets clicked, we can then move to the next question
	nextButton.addEventListener("click", setupQuestions);
};

// Function to display question content to the page
const displayContent = (parent, elementType, html) => {
	const temp = document.createElement(elementType);
	temp.innerHTML = html;
	parent.append(temp);
	return temp;
};

// Function to display content to page
const setupQuestions = () => {
	// Clear output
	outPut.innerHTML = "";

	// Loop through question in game object's question array
	let question = game.questions[game.question];
	game.question++;

	// check to see if there are still questions
	if (game.question <= game.questions.length) {
		// Setup answers array
		let answers = question.incorrect_answers;
		answers.push(question.correct_answer);

		// Suffle array items so correct answer is in a different spot each time
		answers.sort(() => Math.random() - 0.5);

		// Display question number
		const countMessage = `Question #${game.question} of ${game.questions.length}`;

		// Call function to display question content to the page
		const mainDiv = displayContent(outPut, "div", "");
		const questionCount = displayContent(mainDiv, "h3", countMessage);
		const que1 = displayContent(mainDiv, "div", `${question.question}`);
		const answersDiv = displayContent(outPut, "div", "");

		// Clear game elements array before creating new elements
		game.elements.length = 0;

		// Call function to display answers to question
		answers.forEach((answer) => {
			// Setup answer and pass it to function for rendering
			const answer1 = displayContent(answersDiv, "button", `${answer}`);
			answer1.classList.add("btn-1");

			// Setup correct and incorrect messages to the user
			const correctMsg = `Correct! Great Job knowing the answer, "${question.correct_answer}"!`;
			const incorrectMsg = `Sorry, that is incorrect. The correct answer is "${question.correct_answer}"`;

			// Add all answers to game.elements array
			game.elements.push(answer1);

			// Setup Event listener for function that reacts to clicks.
			answer1.addEventListener("click", (e) => {
				game.elements.forEach((el) => {
					el.disabled = true;
					el.classList.add("display-none");
				});
				if (answer === question.correct_answer) {
					// Add correct answer styling to the correct answer CTA if and when user selects it
					answer1.classList.add("right-answer");
					answer1.textContent = "That's RIGHT!";

					// Increment game.correct everytime the user answers a question correctly.
					game.correct++;

					// Display correct message to the user when user answers question correctly
					const msg = displayContent(answersDiv, "h3", correctMsg);

					// Call function to move user to the next question
					nextQuestion(answersDiv);
				} else {
					// Add incorrect answer styling to incorrect answer CTA if and when user selects it
					answer1.classList.add("wrong-answer");
					answer1.textContent = "WRONG X";

					// Increment game.incorrect everytime the user answers a question incorrectly.
					game.incorrect++;

					// Display incorrect message to the user when user answers question incorrectly
					const msg = displayContent(answersDiv, "h3", incorrectMsg);

					// Call function to move user to the next question
					nextQuestion(answersDiv);
				}
			});
		});
	}
	// If no more questions left, display user's game stats,a game over message and ask user if they want to start a new game.
	else if (game.question > game.questions.length) {
		// Create and configure Game Over message and Game Stats to display to the user
		const gameFinished = document.createElement("h3");
		const gameStats = document.createElement("h3");
		const category = game.category !== "" ? game.category : "General Knowledge";
		const level = game.level !== "" ? game.level : "Random";

		// Game Over message text content
		gameFinished.textContent = "Game Over";

		// Setup game stats message that gets displayed to the user
		gameStats.innerHTML = `  <h4><u>Game Stats:</u></h4><br>
                                 <h4>Category: ${category}</h4><br>
                                 <h4>Difficulty: ${level}</h4><br>
                                 <h4>Number of Question(s): ${game.questions.length}</h4><br>
                                 <h4>Number Correct Answer(s): ${game.correct}</h4><br>
                                 <h4>Number Incorrect Answer(s): ${game.incorrect}</h4><br>`;

		// Create "New Game" button that user can click to start a new game
		const playAgain = document.createElement("button");
		playAgain.classList.add("btn-1");
		playAgain.classList.add("start-game");
		playAgain.textContent = "New Game";

		// Append Game Over message, Game Stats and New Game button to output div for user to view
		outPut.append(gameFinished);
		outPut.append(gameStats);
		outPut.append(playAgain);

		// Event Listener for New Game button that reloads page once user clicks it
		playAgain.addEventListener("click", () => {
			window.location.reload();
		});
	}
};

// Function to get content from URL
const getContent = (url) => {
	// Make Fetch Request

	fetch(url)
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			//
			// Get results array from API
			const { results } = data;

			//Load return results array game object's questions array
			game.questions = results;

			// call function to setup questions
			setupQuestions();
		});
};

// Event Listeners
btn.addEventListener("click", btnHandler);
