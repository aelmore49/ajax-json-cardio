// Chuck Norris Joke API App

// API URL
const url = `https://api.chucknorris.io/jokes/random?category=`;
const searchUrl = `https://api.chucknorris.io/jokes/search?query=`;

// Elements
const btn = document.querySelector('.btn');
const searchBtn = document.querySelector('.btn-search');
const outPut = document.querySelector('.output');
const selectValue = document.querySelector(".val");
const categories = ["animal", "career", "celebrity", "dev", "explicit", "fashion", "food", "history", "money", "movie", "music", "political", "religion", "science", "sport", "travel"];
const searchInput = document.querySelector(".search");
categories.forEach((c) => {
    const option = document.createElement("option");
    option.value = c;
    option.innerHTML = `<h3>${c}</h3>`;
    option.classList = "options";
    selectValue.append(option);
});

btn.textContent = "Get Chuck Norris Joke!";
searchBtn.textContent = "Search for CHUCK jokes";

// funtion to handle button clicks
const clickHandler = () => {
    outPut.textContent = "";
    const tempUrl = `${url}${selectValue.value}`;
    console.log(tempUrl);
    // Send fetch request
    fetch(tempUrl)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            renderContent(data, "standard");
        })
        .catch((e) => {
            console.log(e);
        });
};

// Search handler function
const searchHandler = () => {
    console.log(searchInput.value);
    if (searchInput.value !== '') {
        outPut.textContent = "";
        const tempUrl = `${searchUrl}${searchInput.value}`;
        fetch(tempUrl)
            .then(res => res.json())
            .then(data => {
                renderContent(data.result, "search");
            });
    } else {
        alert("Enter a search term in");
    }
};


// Render joke to ui
const renderContent = (data, type = "standard") => {

    if (type === "standard") {

        outPut.innerHTML += `<div class="joke-container">
                                <img class="joke-image" src="${data.icon_url}"/>
                                <h2 class="joke">${data.value}</h2>
                                </div>`;
        window.scrollTo({ top: 511 });
    } else if (type === "search") {
        const resultTitle = document.createElement("h2");
        resultTitle.textContent = `Results for Chuck Norris "${searchInput.value}" jokes: ${data.length}`;
        outPut.append(resultTitle);
        data.forEach((result, index) => {


            outPut.innerHTML += `<div class="search-joke-holder"><h3>Number ${index + 1} of ${data.length}</h3>
                                <img src="${result.icon_url}"/>
                                <h2>${result.value}</h2>
                                </div>`;

        });
        searchInput.value = '';
        window.scrollTo({ top: 511 });

    }
};

// click handler for button
btn.addEventListener("click", clickHandler);
searchBtn.addEventListener("click", searchHandler);