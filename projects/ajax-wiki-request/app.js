// AJAX Request to Wikipedia API

const url =
    "https://en.wikipedia.org/w/api.php?action=query&format=json&list=search&origin=*&srsearch=;";
const btn = document.querySelector(".btn");
const output = document.querySelector(".output");
const inputVal = document.querySelector(".val");
let attempCounter = false;
btn.textContent = "Load JSON data";

const handleRequest = (e) => {
    let searchTerm = inputVal.value || "javascript";

    let tempUrl = url + searchTerm;
    fetch(tempUrl)
        .then((res) => res.json())
        .then((data) => {
            const searchData = data.query;
            outputData(searchData, searchTerm);
        })
        .catch((e) => {
            console.log(e);
        });
};

const outputData = (data, search) => {
    for (const key in data) {
        output.innerHTML = `<h2>Results for: <span class="search-term">${search}</span></h2>`;
        if (Object.hasOwnProperty.call(data, key)) {
            const searchArray = data.search;
            if (searchArray.length) {
                searchArray.forEach((result, index) => {
                    console.log(result);
                    let contentContainer = document.createElement("div");
                    contentContainer.innerHTML += `<a href="https://en.wikipedia.org/wiki?curid=${result.pageid}" target="_blank">
                                                    <div><h2>Title:</h2> <h3>${result.title}</h3></div>
                                                    <div> <h2>Word Count:</h2> <p>${result.wordcount}</p></div>
                                                    <div><h2> Overview: </h2><p><i>${result.snippet}</i></p></div>
                                                   </a>`;
                    contentContainer.classList.add("myData");
                    output.append(contentContainer);
                });
            }
        }
    }
};

btn.addEventListener("click", handleRequest);
